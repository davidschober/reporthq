using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;

namespace ReportHq.Common
{
	/// <summary>
	/// Helper utility for doing email tasks.
	/// </summary>
	public class Email
	{
		#region Variables


		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Properties


		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Constructors

		public Email()
		{

		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		#endregion

		#region Public Methods

		/// <summary>
		/// Sends an email using the .config's mail settings.
		/// </summary>
		public static StatusInfo SendEmail(string from, string to, string subject, string body, bool isHtml)
		{
			try
			{
				MailMessage message = new MailMessage(from, to, subject, body);
				message.IsBodyHtml = isHtml;

				SmtpClient smtpClient = new SmtpClient(); //pulls the mail relay server address from the app's .config (<mailSettings>)

				smtpClient.Send(message);

				return new StatusInfo(true);

				//put this in .config file:
				//
				//<system.net>
				//    <mailSettings>
				//        <smtp>
				//            <network host="MNORelay02" />
				//        </smtp>
				//    </mailSettings>
				//</system.net>

			}
			catch (SmtpException smtpEx) //problem occurred when sending the message
			{
				return new StatusInfo(false, "SMTP error: " + smtpEx.Message);
			}
			catch (Exception ex) //some other problem
			{
				return new StatusInfo(false, ex.Message);
			}

			//NOTE: core problems in sending mail are often in the ex.InnerException
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Pulls up an email template (.HTML file) from a URL. App coder can then swap out data into template's {placeholders}.
		/// </summary>
		public static string GetEmailBody(string url)
		{
			WebRequest request = HttpWebRequest.Create(url);
			WebResponse response = request.GetResponse();

			//Read response to a string
			Stream stream = response.GetResponseStream();
			StreamReader sr = new StreamReader(stream);

			string html = sr.ReadToEnd();
			sr.Close();

			return html;
		}

		#endregion
	}
}
