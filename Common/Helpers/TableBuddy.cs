using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace ReportHq.Common
{
	/// <summary>
	/// Prototype to test comparing two same-schema datatables.
	/// </summary>
	public class TableBuddy
	{
		#region Enums

		enum CellAlignment
		{
			Left = 1,
			Center = 2,
			Right = 3
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Variables

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	
		#endregion

		#region Properties

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		#endregion

		#region Constructor
	
		public TableBuddy()
		{
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		/// <summary>
		/// Converts an ADO.NET table into an HTML one.
		/// </summary>
		/// <param name="dt">The table.</param>
		/// <param name="noWraps">A string of characters that represent each column; 0 = false, 1 = true. Ex: "1110", first three columns won't wrap, last one will.</param>
		/// <param name="alignments">A string of characters that represent each column; 1 = Left, 2 = Center, 3 = Right. Ex: "1113", first three columns left-aligned, last one right-aligned.</param>
		/// <returns>String of table.</returns>
		public static string ConvertDataTableToHtml(DataTable dt, string noWraps, string alignments)
		{
			//use linq to convert each character into a boolean, into array variable
			bool[] bNoWraps = noWraps.Select(chr => chr == '1').ToArray();

			//use linq to convert each character into our enum type, into array variable. c-0 returns the digit of c.
			CellAlignment[] cellAlignments = alignments.Select(c => (CellAlignment)(c - '0')).ToArray();


			//build table
			HtmlTable table = new HtmlTable();
			HtmlTableRow row = new HtmlTableRow();
			HtmlTableCell cell;

			//build header row
			foreach (DataColumn col in dt.Columns)
			{
				cell = new HtmlTableCell("th");
				cell.InnerText = col.Caption;

				row.Cells.Add(cell); //add cell to the row
			}

			//add row to the table
			table.Rows.Add(row);

			//build data rows
			int i = 0;
			int i2 = 0;
			foreach (DataRow dr in dt.Rows)
			{
				row = new HtmlTableRow();

				//if even row, append class="alt" to <tr>
				if (i % 2 != 0)
				{
					row.Attributes.Add("class", "alt");
				}

				//cells
				for (i2 = 0; i2 <= dr.ItemArray.Length - 1; i2++)
				{
					cell = new HtmlTableCell();
					cell.NoWrap = bNoWraps[i2];
					cell.InnerHtml = dr.ItemArray[i2].ToString();

					//set alignment
					cell.Align = cellAlignments[i2].ToString();

					row.Cells.Add(cell);
				}

				//add row to the table
				table.Rows.Add(row);

				i++; //increment our odd/even counter
			}

			//render htmltable to stringbuilder
			StringBuilder sb = new StringBuilder();
			StringWriter sw = new StringWriter(sb);
			HtmlTextWriter tw = new HtmlTextWriter(sw);
			table.RenderControl(tw);

			return sb.ToString();
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Compares two same-schema tables.
		/// </summary>
		public static DataSet CompareTables(DataTable firstTable, DataTable secondTable, int rowUiquenessColumnID)
		{
			DataSet results = new DataSet();

			//added rows
			results.Tables.Add( RemoveModifications(GetTableDifferences(firstTable, secondTable), secondTable, 0, false) );

			//deleted rows
			results.Tables.Add( RemoveModifications(GetTableDifferences(secondTable, firstTable), firstTable, 0, false) );

			//modified rows
			results.Tables.Add( RemoveModifications(GetTableDifferences(firstTable, secondTable), secondTable, 0, true) );

			return results;
		}
			
		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Loops thru a Table and returns a string of values for the designated column. ex: Minnnesota,California,Louisiana
		/// </summary>
		public static string ConcatColumnRowsIntoString(DataTable table, int columnIndexToConcat)
		{
			//string returnValue = string.Empty;
			StringBuilder sb = new StringBuilder(200);

			foreach (DataRow row in table.Rows)
			{
				sb.Append(row[columnIndexToConcat].ToString() + ",");
			}

			return sb.ToString().TrimEnd(new Char[] { ',' });
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		/// <summary>
		/// Returns all rows that are in First table but not Second. Tables must have same schema.
		/// Summary: 
		///		- creates empty results table
		/// 	- creates DataSet and adds tables
		/// 	- gets reference to all columns in both tables
		/// 	- creates DataRelation
		/// 	- uses DataRelation to add rows with no corresponding child rows
		/// 	- returns results
		/// </summary>
		private static DataTable GetTableDifferences(DataTable firstTable, DataTable secondTable)
		{
			DataTable dtResults = new DataTable();

			//use Dataset to make use of DataRelation object
			using(DataSet ds = new DataSet())
			{
				firstTable.TableName = "FirstTable";
				secondTable.TableName = "SecondTable";

				//add tables
				ds.Tables.AddRange(new DataTable[] { firstTable.Copy(), secondTable.Copy() } );

				//get columns for DataRelation
				DataColumn[] firstColumns  = new DataColumn[ds.Tables[0].Columns.Count];
				for (int i = 0; i < firstColumns.Length; i++)
				{
					firstColumns[i] = ds.Tables[0].Columns[i];
				}

				DataColumn[] secondColumns = new DataColumn[ds.Tables[1].Columns.Count];
				for (int i = 0; i < secondColumns.Length; i++)
				{
					secondColumns[i] = ds.Tables[1].Columns[i];
				}

				//create DataRelation
				DataRelation r = new DataRelation(string.Empty, firstColumns, secondColumns, false);
				ds.Relations.Add(r);


				//create return table's columns
				for (int i = 0; i < firstTable.Columns.Count; i++)
				{
					dtResults.Columns.Add(firstTable.Columns[i].ColumnName, firstTable.Columns[i].DataType);
				}
 

				//if First row not in Second, add to return table
				dtResults.BeginLoadData();

				foreach(DataRow parentRow in ds.Tables[0].Rows) //firstTable
				{
					DataRow[] childRows = parentRow.GetChildRows(r);

					if (childRows == null || childRows.Length == 0)
					{
						dtResults.LoadDataRow(parentRow.ItemArray, true);
					}
				}

				dtResults.EndLoadData();
			}

			return dtResults;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		/// <summary>
		/// Takes in a "differences" table, and removes modifications by cross-referencing w/ a "source" table.
		/// If this is called w/ returnModifications = true, then it returns the "modification" rows themselves.
		/// </summary>
		private static DataTable RemoveModifications(DataTable differencesTable, DataTable sourceTable, int rowUiquenessColumnID, bool returnModifications)
		{
			#region prep return table
			DataTable dtModifiedRows = null;

			if (returnModifications)
			{
				dtModifiedRows = new DataTable("ModifiedRows");
			
				//Create columns for return table
				for(int i=0; i < sourceTable.Columns.Count; i++)
					dtModifiedRows.Columns.Add(sourceTable.Columns[i].ColumnName, sourceTable.Columns[i].DataType);
			}
			#endregion

			//loop thru differences table
			foreach (DataRow differenceRow in differencesTable.Rows)
			{
				//get primary key value
				object uniqueRowID = differenceRow[rowUiquenessColumnID];

				//loop thru source table
				foreach (DataRow sourceRow in sourceTable.Rows)
				{
					//if unique row ID found in source, this row is a modification
					if (Object.Equals(sourceRow[rowUiquenessColumnID], uniqueRowID))
					{
						if (returnModifications)
						{
							dtModifiedRows.LoadDataRow(differenceRow.ItemArray, true); //copy to modified table
						}

						differenceRow.Delete();						
						break;
					}
				}
			}

			if (returnModifications)
				return dtModifiedRows;
			else
				return differencesTable;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
