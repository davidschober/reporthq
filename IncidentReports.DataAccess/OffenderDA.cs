using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using ReportHq.Common;

namespace ReportHq.IncidentReports.DataAccess
{
	/// <summary>
	/// DA layer for the Offender object.
	/// </summary>
	public class OffenderDA
	{
		#region Variables

		static private string _connStr = ConfigurationManager.ConnectionStrings["ReportHq"].ConnectionString;

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		public static StatusInfo InsertOffender(int reportID,
												int offenderNumber,
												int statusTypeID,
												string lastName,
												string firstName,
												string nickname,
												int raceTypeID,
												int genderTypeID,
												DateTime dateOfBirth,
												int height,
												int weight,
												string streetAddress,
												string city,
												string state,
												int zipCode,
												string socialSecurityNumber,
												string driversLicenseNumber,
												int sobrietyTypeID,
												int injurtyTypeID,
												int treatedTypeID,
												int arrestTypeID,
												string arrestLocation,
												DateTime arrestDateTime,
												string arrestCredit,
												string transportedBy,
												string transportUnit,
												int rightsWaivedFormNumber,
												int residentTypeID,
												int juvenileDispositionTypeID,
												int district,
												string zone,
												string subZone,
												string createdBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("InsertOffender", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@offenderNumber", SqlDbType.Int).Value = offenderNumber;
			command.Parameters.Add("@offenderStatusTypeID", SqlDbType.Int).Value = statusTypeID;
			command.Parameters.Add("@createdBy", SqlDbType.NVarChar, 50).Value = createdBy;

			//optional
			if (!string.IsNullOrEmpty(lastName))
				command.Parameters.Add("@lastName", SqlDbType.NVarChar, 50).Value = lastName;

			if (!string.IsNullOrEmpty(firstName))
				command.Parameters.Add("@firstName", SqlDbType.NVarChar, 50).Value = firstName;

			if (!string.IsNullOrEmpty(nickname))
				command.Parameters.Add("@nickname", SqlDbType.NVarChar, 50).Value = nickname;

			if (raceTypeID > 0)
				command.Parameters.Add("@raceTypeID", SqlDbType.Int).Value = raceTypeID;

			if (genderTypeID > 0)
				command.Parameters.Add("@genderTypeID", SqlDbType.Int).Value = genderTypeID;

			if (dateOfBirth > new DateTime(1900, 1, 1))
				command.Parameters.Add("@dateOfBirth", SqlDbType.Date).Value = dateOfBirth;
			
			if (height > 0)
				command.Parameters.Add("@height", SqlDbType.Int).Value = height;

			if (weight > 0)
				command.Parameters.Add("@weight", SqlDbType.Int).Value = weight;
			
			if (!string.IsNullOrEmpty(streetAddress))
				command.Parameters.Add("@streetAddress", SqlDbType.NVarChar, 50).Value = streetAddress;

			if (!string.IsNullOrEmpty(city))
				command.Parameters.Add("@city", SqlDbType.NVarChar, 50).Value = city;

			if (!string.IsNullOrEmpty(state))
				command.Parameters.Add("@state", SqlDbType.NVarChar, 2).Value = state;

			if (zipCode > 0)
				command.Parameters.Add("@zipCode", SqlDbType.Int).Value = zipCode;

			if (!string.IsNullOrEmpty(socialSecurityNumber))
				command.Parameters.Add("@socialSecurityNumber", SqlDbType.NVarChar, 9).Value = socialSecurityNumber;

			if (!string.IsNullOrEmpty(driversLicenseNumber))
				command.Parameters.Add("@driversLicenseNumber", SqlDbType.NVarChar, 20).Value = driversLicenseNumber;

			if (sobrietyTypeID > 0)
				command.Parameters.Add("@sobrietyTypeID", SqlDbType.Int).Value = sobrietyTypeID;

			if (injurtyTypeID > 0)
				command.Parameters.Add("@injuryTypeID", SqlDbType.Int).Value = injurtyTypeID;

			if (treatedTypeID > 0)
				command.Parameters.Add("@treatedTypeID", SqlDbType.Int).Value = treatedTypeID;

			if (arrestTypeID> 0)
				command.Parameters.Add("@arrestTypeID", SqlDbType.Int).Value = arrestTypeID;

			if (!string.IsNullOrEmpty(arrestLocation))
				command.Parameters.Add("@arrestLocation", SqlDbType.NVarChar, 50).Value = arrestLocation;

			if (arrestDateTime > new DateTime(1900, 1, 1))
				command.Parameters.Add("@arrestDate", SqlDbType.DateTime).Value = arrestDateTime;

			if (!string.IsNullOrEmpty(arrestCredit))
				command.Parameters.Add("@arrestCredit", SqlDbType.NVarChar, 50).Value = arrestCredit;

			if (!string.IsNullOrEmpty(transportedBy))
				command.Parameters.Add("@transportedBy", SqlDbType.NVarChar, 50).Value = transportedBy;

			if (!string.IsNullOrEmpty(transportUnit))
				command.Parameters.Add("@transportUnit", SqlDbType.NVarChar, 50).Value = transportUnit;

			if (rightsWaivedFormNumber > 0)
				command.Parameters.Add("@rightsWaivedFormNumber", SqlDbType.Int).Value = rightsWaivedFormNumber;

			if (residentTypeID > 0)
				command.Parameters.Add("@residentTypeID", SqlDbType.Int).Value = residentTypeID;
			
			if (juvenileDispositionTypeID > 0)
				command.Parameters.Add("@juvenileDispositionTypeID", SqlDbType.Int).Value = juvenileDispositionTypeID;

			if (district > 0)
				command.Parameters.Add("@district", SqlDbType.Int).Value = district;

			if (!string.IsNullOrEmpty(zone))
				command.Parameters.Add("@zone", SqlDbType.NChar, 1).Value = zone;

			if (!string.IsNullOrEmpty(subZone))
				command.Parameters.Add("@subZone", SqlDbType.NChar, 2).Value = subZone;

			//output - new VictimPersonID
			SqlParameter param = new SqlParameter("@offenderID", SqlDbType.Int);
			param.Direction = ParameterDirection.Output;
			command.Parameters.Add(param);

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				//send back new ID
				return new StatusInfo(true, (int)param.Value);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo UpdateOffender(int offenderID,
												int offenderNumber,
												int statusTypeID,
												string lastName,
												string firstName,
												string nickname,
												int raceTypeID,
												int genderTypeID,
												DateTime dateOfBirth,
												int height,
												int weight,
												string streetAddress,
												string city,
												string state,
												int zipCode,
												string socialSecurityNumber,
												string driversLicenseNumber,
												int sobrietyTypeID,
												int injurtyTypeID,
												int treatedTypeID,
												int arrestTypeID,
												string arrestLocation,
												DateTime arrestDateTime,
												string arrestCredit,
												string transportedBy,
												string transportUnit,
												int rightsWaivedFormNumber,
												int residentTypeID,
												int juvenileDispositionTypeID,
												int district,
												string zone,
												string subZone,
												string modifiedBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateOffender", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@offenderID", SqlDbType.Int).Value = offenderID;
			command.Parameters.Add("@offenderNumber", SqlDbType.Int).Value = offenderNumber;
			command.Parameters.Add("@offenderStatusTypeID", SqlDbType.Int).Value = statusTypeID;
			command.Parameters.Add("@modifiedBy", SqlDbType.NVarChar, 50).Value = modifiedBy;

			//optional
			if (!string.IsNullOrEmpty(lastName))
				command.Parameters.Add("@lastName", SqlDbType.NVarChar, 50).Value = lastName;

			if (!string.IsNullOrEmpty(firstName))
				command.Parameters.Add("@firstName", SqlDbType.NVarChar, 50).Value = firstName;

			if (!string.IsNullOrEmpty(nickname))
				command.Parameters.Add("@nickname", SqlDbType.NVarChar, 50).Value = nickname;

			if (raceTypeID > 0)
				command.Parameters.Add("@raceTypeID", SqlDbType.Int).Value = raceTypeID;

			if (genderTypeID > 0)
				command.Parameters.Add("@genderTypeID", SqlDbType.Int).Value = genderTypeID;

			if (dateOfBirth > new DateTime(1900, 1, 1))
				command.Parameters.Add("@dateOfBirth", SqlDbType.Date).Value = dateOfBirth;

			if (height > 0)
				command.Parameters.Add("@height", SqlDbType.Int).Value = height;

			if (weight > 0)
				command.Parameters.Add("@weight", SqlDbType.Int).Value = weight;

			if (!string.IsNullOrEmpty(streetAddress))
				command.Parameters.Add("@streetAddress", SqlDbType.NVarChar, 50).Value = streetAddress;

			if (!string.IsNullOrEmpty(city))
				command.Parameters.Add("@city", SqlDbType.NVarChar, 50).Value = city;

			if (!string.IsNullOrEmpty(state))
				command.Parameters.Add("@state", SqlDbType.NVarChar, 2).Value = state;

			if (zipCode > 0)
				command.Parameters.Add("@zipCode", SqlDbType.Int).Value = zipCode;

			if (!string.IsNullOrEmpty(socialSecurityNumber))
				command.Parameters.Add("@socialSecurityNumber", SqlDbType.NVarChar, 9).Value = socialSecurityNumber;

			if (!string.IsNullOrEmpty(driversLicenseNumber))
				command.Parameters.Add("@driversLicenseNumber", SqlDbType.NVarChar, 20).Value = driversLicenseNumber;

			if (sobrietyTypeID > 0)
				command.Parameters.Add("@sobrietyTypeID", SqlDbType.Int).Value = sobrietyTypeID;

			if (injurtyTypeID > 0)
				command.Parameters.Add("@injuryTypeID", SqlDbType.Int).Value = injurtyTypeID;

			if (treatedTypeID > 0)
				command.Parameters.Add("@treatedTypeID", SqlDbType.Int).Value = treatedTypeID;

			if (arrestTypeID > 0)
				command.Parameters.Add("@arrestTypeID", SqlDbType.Int).Value = arrestTypeID;

			if (!string.IsNullOrEmpty(arrestLocation))
				command.Parameters.Add("@arrestLocation", SqlDbType.NVarChar, 50).Value = arrestLocation;

			if (arrestDateTime > new DateTime(1900, 1, 1))
				command.Parameters.Add("@arrestDate", SqlDbType.DateTime).Value = arrestDateTime;

			if (!string.IsNullOrEmpty(arrestCredit))
				command.Parameters.Add("@arrestCredit", SqlDbType.NVarChar, 50).Value = arrestCredit;

			if (!string.IsNullOrEmpty(transportedBy))
				command.Parameters.Add("@transportedBy", SqlDbType.NVarChar, 50).Value = transportedBy;

			if (!string.IsNullOrEmpty(transportUnit))
				command.Parameters.Add("@transportUnit", SqlDbType.NVarChar, 50).Value = transportUnit;

			if (rightsWaivedFormNumber > 0)
				command.Parameters.Add("@rightsWaivedFormNumber", SqlDbType.Int).Value = rightsWaivedFormNumber;

			if (residentTypeID > 0)
				command.Parameters.Add("@residentTypeID", SqlDbType.Int).Value = residentTypeID;

			if (juvenileDispositionTypeID > 0)
				command.Parameters.Add("@juvenileDispositionTypeID", SqlDbType.Int).Value = juvenileDispositionTypeID;

			if (district > 0)
				command.Parameters.Add("@district", SqlDbType.Int).Value = district;

			if (!string.IsNullOrEmpty(zone))
				command.Parameters.Add("@zone", SqlDbType.NChar, 1).Value = zone;

			if (!string.IsNullOrEmpty(subZone))
				command.Parameters.Add("@subZone", SqlDbType.NChar, 2).Value = subZone;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();
			
				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}

		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo DeleteOffender(int offenderID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("DeleteOffender", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@offenderID", SqlDbType.Int).Value = offenderID;

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();
			
				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo UpdateOffenderDescriptors(int offenderID, string itemIdsXml, string additionalDescription, string modifiedBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateOffenderDescriptors", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@offenderID", SqlDbType.Int).Value = offenderID;
			command.Parameters.Add("@itemIds", SqlDbType.Xml).Value = itemIdsXml;
			command.Parameters.Add("@modifiedBy", SqlDbType.NVarChar, 50).Value = modifiedBy;

			if (!string.IsNullOrEmpty(additionalDescription))
				command.Parameters.Add("@additionalDescription", SqlDbType.NVarChar, 200).Value = additionalDescription;


			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo InsertDomesticViolenceOffender(int reportID,
																int offenderID,
																int medicalTreatmentTypeID,
																string medicalUnitNumber,
																string medicalAddress,
																bool madeComments,
																string comments,
																string descriptorsXml,
																string injuriesXml,
																string createdBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("InsertDomesticViolenceOffender", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@offenderID", SqlDbType.Int).Value = offenderID;
			command.Parameters.Add("@createdBy", SqlDbType.NVarChar, 50).Value = createdBy;

			//optional
			if (medicalTreatmentTypeID > 0)
				command.Parameters.Add("@medicalTreatmentTypeID", SqlDbType.Int).Value = medicalTreatmentTypeID;

			if (!string.IsNullOrEmpty(medicalUnitNumber))
				command.Parameters.Add("@medicalUnitNumber", SqlDbType.NVarChar, 10).Value = medicalUnitNumber;

			if (!string.IsNullOrEmpty(medicalAddress))
				command.Parameters.Add("@medicalAddress", SqlDbType.NVarChar, 200).Value = medicalAddress;

			command.Parameters.Add("@madeComments", SqlDbType.Bit).Value = madeComments;
			command.Parameters.Add("@comments", SqlDbType.NVarChar, 2000).Value = comments;
			command.Parameters.Add("@descriptorItemIds", SqlDbType.Xml).Value = descriptorsXml;
			command.Parameters.Add("@injuryItemIds", SqlDbType.Xml).Value = injuriesXml;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				//send back new ID
				//return new StatusInfo(true, (int)param.Value);
				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo UpdateDomesticViolenceOffender(int offenderID,
																int medicalTreatmentTypeID,
																string medicalUnitNumber,
																string medicalAddress,
																bool madeComments,
																string comments,
																string descriptorsXml,
																string injuriesXml,
																string modifiedBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateDomesticViolenceOffender", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@offenderID", SqlDbType.Int).Value = offenderID;
			command.Parameters.Add("@lastModifiedBy", SqlDbType.NVarChar, 50).Value = modifiedBy;

			//optional
			if (medicalTreatmentTypeID > 0)
				command.Parameters.Add("@medicalTreatmentTypeID", SqlDbType.Int).Value = medicalTreatmentTypeID;

			if (!string.IsNullOrEmpty(medicalUnitNumber))
				command.Parameters.Add("@medicalUnitNumber", SqlDbType.NVarChar, 10).Value = medicalUnitNumber;

			if (!string.IsNullOrEmpty(medicalAddress))
				command.Parameters.Add("@medicalAddress", SqlDbType.NVarChar, 200).Value = medicalAddress;

			command.Parameters.Add("@madeComments", SqlDbType.Bit).Value = madeComments;
			command.Parameters.Add("@comments", SqlDbType.NVarChar, 2000).Value = comments;
			command.Parameters.Add("@descriptorItemIds", SqlDbType.Xml).Value = descriptorsXml;
			command.Parameters.Add("@injuryItemIds", SqlDbType.Xml).Value = injuriesXml;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				//send back new ID
				//return new StatusInfo(true, (int)param.Value);
				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo DeleteDomesticViolenceOffender(int offenderID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("DeleteDomesticViolenceOffender", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@offenderID", SqlDbType.Int).Value = offenderID;

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataSet GetOffenderByID(int offenderID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetOffenderByID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@offenderID", SqlDbType.Int).Value = offenderID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetOffenderStatusTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetOffenderStatusTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetArrestTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetArrestTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetJuvenileDispositionTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetJuvenileDispositionTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetOffendersByReport(int reportID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetOffendersByReportID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetOffendersByCriteria(string firstName, 
													   string lastName, 
													   string nickname, 
													   string driversLicenseNumber, 
													   string socialSecurityNumber, 
													   int raceTypeID, 
													   int genderTypeID, 
													   int height, 
													   int weight,
													   string descriptorsXml)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetOffendersByCriteria", conn);
			command.CommandType = CommandType.StoredProcedure;
			command.CommandTimeout = 120;

			#region params

			if (!string.IsNullOrEmpty(firstName))
				command.Parameters.Add("@firstName", SqlDbType.NVarChar, 50).Value = firstName;

			if (!string.IsNullOrEmpty(lastName))
				command.Parameters.Add("@lastName", SqlDbType.NVarChar, 50).Value = lastName;

			if (!string.IsNullOrEmpty(nickname))
				command.Parameters.Add("@nickname", SqlDbType.NVarChar, 50).Value = nickname;

			if (!string.IsNullOrEmpty(driversLicenseNumber))
				command.Parameters.Add("@driversLicenseNumber", SqlDbType.NVarChar, 20).Value = driversLicenseNumber;

			if (!string.IsNullOrEmpty(socialSecurityNumber))
				command.Parameters.Add("@socialSecurityNumber", SqlDbType.NChar, 9).Value = socialSecurityNumber;
			
			if (raceTypeID > 0)
				command.Parameters.Add("@raceTypeID", SqlDbType.Int).Value = raceTypeID;

			if (genderTypeID > 0)
				command.Parameters.Add("@genderTypeID", SqlDbType.Int).Value = genderTypeID;

			if (height > 0)
				command.Parameters.Add("@height", SqlDbType.Int).Value = height;

			if (weight > 0)
				command.Parameters.Add("@weight", SqlDbType.Int).Value = weight;

			if (!string.IsNullOrEmpty(descriptorsXml))
				command.Parameters.Add("@itemIds", SqlDbType.Xml).Value = descriptorsXml;
			
			#endregion

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetOffenderDescriptorTypesByCategory(int categoryID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetOffenderDescriptorTypesByCategoryID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@categoryID", SqlDbType.Int).Value = categoryID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetOffenderDescriptorsByCategory(int offenderID, int categoryID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetOffenderDescriptorsByCategoryID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@offenderID", SqlDbType.Int).Value = offenderID;
			command.Parameters.Add("@categoryID", SqlDbType.Int).Value = categoryID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetDomesticViolenceOffendersByReport(int reportID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetDomesticViolenceOffendersByReportID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetDomesticViolenceDescriptorsByOffender(int offenderID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetDomesticViolenceDescriptorsByOffenderID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@offenderID", SqlDbType.Int).Value = offenderID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetDomesticViolenceOffenderInjuries(int offenderID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetDomesticViolenceInjuriesByOffenderID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@offenderID", SqlDbType.Int).Value = offenderID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


		#endregion
	}	
}
