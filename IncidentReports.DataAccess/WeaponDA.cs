using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using ReportHq.Common;

namespace ReportHq.IncidentReports.DataAccess
{
	/// <summary>
	/// DA layer for the Weapon object.
	/// </summary>
	public class WeaponDA
	{
		#region Variables

		static private string _connStr = ConfigurationManager.ConnectionStrings["ReportHq"].ConnectionString;

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		public static StatusInfo InsertWeapon(int reportID,
											  int firearmTypeID,
											  int firearmMakeTypeID,
											  string model,
											  int firearmCaliberTypeID,
											  string serialNumber,
											  bool ncicIsStolen,
											  string ncicContactName,
											  bool hasPawnshopRecord,
											  string pawnshopContactName,
											  string additionalInfo,
											  string createdBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("InsertWeapon", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@firearmTypeID", SqlDbType.Int).Value = firearmTypeID;
			command.Parameters.Add("@createdBy", SqlDbType.NVarChar, 50).Value = createdBy;

			//optional
			if (firearmMakeTypeID > 0)
				command.Parameters.Add("@firearmMakeTypeID", SqlDbType.Int).Value = firearmMakeTypeID;

			if (!string.IsNullOrEmpty(model))
				command.Parameters.Add("@model", SqlDbType.NVarChar, 50).Value = model;

			if (firearmCaliberTypeID > 0)
				command.Parameters.Add("@firearmCaliberTypeID", SqlDbType.Int).Value = firearmCaliberTypeID;

			if (!string.IsNullOrEmpty(serialNumber))
				command.Parameters.Add("@serialNumber", SqlDbType.NVarChar, 50).Value = serialNumber;

			command.Parameters.Add("@ncicIsStolen", SqlDbType.Bit).Value = ncicIsStolen;

			if (!string.IsNullOrEmpty(ncicContactName))
				command.Parameters.Add("@ncicContactName", SqlDbType.NVarChar, 50).Value = ncicContactName;
			
			command.Parameters.Add("@hasPawnshopRecord", SqlDbType.Bit).Value = hasPawnshopRecord;

			if (!string.IsNullOrEmpty(pawnshopContactName))
				command.Parameters.Add("@pawnshopContactName", SqlDbType.NVarChar, 50).Value = pawnshopContactName;

			if (!string.IsNullOrEmpty(additionalInfo))
				command.Parameters.Add("@additionalInfo", SqlDbType.NVarChar, 100).Value = additionalInfo;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				//send back new ID
				//return new StatusInfo(true, (int)param.Value);
				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo UpdateWeapon(int weaponID,
											  int firearmTypeID,
											  int firearmMakeTypeID,
											  string model,
											  int firearmCaliberTypeID,
											  string serialNumber,
											  bool ncicIsStolen,
											  string ncicContactName,
											  bool hasPawnshopRecord,
											  string pawnshopContactName,
											  string additionalInfo,
											  string modifiedBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateWeapon", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@weaponID", SqlDbType.Int).Value = weaponID;
			command.Parameters.Add("@firearmTypeID", SqlDbType.Int).Value = firearmTypeID;
			command.Parameters.Add("@lastModifiedBy", SqlDbType.NVarChar, 50).Value = modifiedBy;

			//optional
			if (firearmMakeTypeID > 0)
				command.Parameters.Add("@firearmMakeTypeID", SqlDbType.Int).Value = firearmMakeTypeID;

			if (!string.IsNullOrEmpty(model))
				command.Parameters.Add("@model", SqlDbType.NVarChar, 50).Value = model;

			if (firearmCaliberTypeID > 0)
				command.Parameters.Add("@firearmCaliberTypeID", SqlDbType.Int).Value = firearmCaliberTypeID;

			if (!string.IsNullOrEmpty(serialNumber))
				command.Parameters.Add("@serialNumber", SqlDbType.NVarChar, 50).Value = serialNumber;

			command.Parameters.Add("@ncicIsStolen", SqlDbType.Bit).Value = ncicIsStolen;

			if (!string.IsNullOrEmpty(ncicContactName))
				command.Parameters.Add("@ncicContactName", SqlDbType.NVarChar, 50).Value = ncicContactName;

			command.Parameters.Add("@hasPawnshopRecord", SqlDbType.Bit).Value = hasPawnshopRecord;

			if (!string.IsNullOrEmpty(pawnshopContactName))
				command.Parameters.Add("@pawnshopContactName", SqlDbType.NVarChar, 50).Value = pawnshopContactName;

			if (!string.IsNullOrEmpty(additionalInfo))
				command.Parameters.Add("@additionalInfo", SqlDbType.NVarChar, 100).Value = additionalInfo;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo DeleteWeapon(int weaponID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("DeleteWeapon", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@weaponID", SqlDbType.Int).Value = weaponID;

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetWeaponsByReport(int reportID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetWeaponsByReportID", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetFirearmTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetFirearmTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetFirearmMakeTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetFirearmMakeTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetFirearmModelTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetFirearmModelTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetFirearmCaliberTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetFirearmCaliberTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}	
}
