﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportHq.IncidentReports.ApiModels
{
	public class Notification
	{
		public int ID { get; set; }
		public string Message { get; set; }
		public int ClickID { get; set; }
		public DateTime CreatedDate { get; set; }
	}
}