using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports.Bases
{
	public class PropertyEvidenceItem
	{
		#region Properties

		private int _ID;
		private int _reportID;
		private int _lossTypeID;
		private string _lossType;
		private decimal _quantity;
		private int _typeID;
		private string _type;
		private int _narcoticWeightTypeID;
		private string _narcoticWeightUnit;
		private int _narcoticTypeID;
		private string _narcoticType;
		private string _description;
		private string _brand;
		private string _serialNumber;
		private int _value;
		
		//private string _createdBy;
		//private DateTime _createdDate;
		//private string _lastModifiedBy;
		//private DateTime _lastModifiedDate;

		public int ID
		{
			set { _ID = value; }
			get { return _ID; }
		}

		public int ReportID
		{
			set { _reportID = value; }
			get { return _reportID; }
		}

		public int LossTypeID
		{
			set { _lossTypeID = value; }
			get { return _lossTypeID; }
		}

		public string LossType
		{
			set { _lossType = value; }
			get { return _lossType; }
		}

		public decimal Quantity
		{
			set { _quantity = value; }
			get { return _quantity; }
		}

		public int TypeID
		{
			set { _typeID = value; }
			get { return _typeID; }
		}

		public string Type
		{
			set { _type = value; }
			get { return _type; }
		}

		public int NarcoticWeightTypeID
		{
			set { _narcoticWeightTypeID = value; }
			get { return _narcoticWeightTypeID; }
		}

		public string NarcoticWeightUnit
		{
			set { _narcoticWeightUnit = value; }
			get { return _narcoticWeightUnit; }
		}

		public int NarcoticTypeID
		{
			set { _narcoticTypeID = value; }
			get { return _narcoticTypeID; }
		}

		public string NarcoticType
		{
			set { _narcoticType = value; }
			get { return _narcoticType; }
		}

		public string Description
		{
			set { _description = value; }
			get { return _description; }
		}

		public string Brand
		{
			set { _brand = value; }
			get { return _brand; }
		}

		public string SerialNumber
		{
			set { _serialNumber = value; }
			get { return _serialNumber; }
		}

		public int Value
		{
			set { _value = value; }
			get { return _value; }
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Static Methods

		public static DataTable GetPropertyTypes()
		{
			const string KEYNAME = "dtPropertyTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = PropertyEvidenceDA.GetPropertyTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetPropertyEvidenceLossTypes()
		{
			const string KEYNAME = "dtPropertyEvidenceLossTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = PropertyEvidenceDA.GetPropertyEvidenceLossTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetNarcoticTypes()
		{
			const string KEYNAME = "dtNarcoticTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = PropertyEvidenceDA.GetNarcoticTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetNarcoticWeightTypes()
		{
			const string KEYNAME = "dtNarcoticWeightTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = PropertyEvidenceDA.GetNarcoticWeightTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
