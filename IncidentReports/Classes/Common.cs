﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

using ReportHq;

namespace ReportHq.IncidentReports
{
	/// <summary>
	/// Shared methods project-wide. (Methods shared solution-wide should go into a new project for common code)
	/// </summary>
	public class Common
	{
		/// <summary>
		/// Gets the running environment as specified by the web.config.
		/// </summary>
		public static Enums.ApplicationEnvironments GetApplicationEnvironment()
		{
			
			string environment = System.Configuration.ConfigurationManager.AppSettings["ApplicationEnvironment"];

			Enums.ApplicationEnvironments returnValue;

			switch (environment)
			{
				case "Production":
					returnValue = Enums.ApplicationEnvironments.Production;
					break;

				case "Training":
					returnValue = Enums.ApplicationEnvironments.Training;
					break;

				case "Test":
					returnValue = Enums.ApplicationEnvironments.Test;
					break;

				case "Development":
					returnValue = Enums.ApplicationEnvironments.Development;
					break;

				default:
					returnValue = Enums.ApplicationEnvironments.Development;
					break;
			}

			return returnValue;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Takes in a LIST of the Report's checked items, and binds to a form's CheckBoxList. Used by checkboxlist UI controls. 
		/// </summary>
		public static void SetCheckedItems(List<DescriptionItem> checkedItems, CheckBoxList checkBoxList)
		{
			//loop thru this report's checked-items
			foreach (DescriptionItem item in checkedItems)
			{
				int itemID = item.ID; //the db xxxTypeID of this report's item

				//loop thru form's checkboxlist items
				foreach (ListItem listItem in checkBoxList.Items)
				{
					if (Int32.Parse(listItem.Value) == itemID) //if listitem's db ID value matches this-item's ID, its in the report. check it, yo!
						listItem.Selected = true;
				}
			}
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Takes in a CheckBoxList, and returns a list of checked items' values (stored in ID prop). Used by checkboxlist UI controls. 
		/// </summary>
		public static List<DescriptionItem> GetCheckedItems(CheckBoxList theList)
		{
			List<DescriptionItem> items = new List<DescriptionItem>();

			//if checked, add to returned list
			foreach (ListItem item in theList.Items)
			{
				if (item.Selected)
					items.Add(new DescriptionItem(Int32.Parse(item.Value)));
			}

			return items;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Builds an xml-string of ID values. Used by our screens that insert multiple checkbox values into db.
		/// </summary>
		public static string GetXmlStringOfIds(List<DescriptionItem> items)
		{
			StringBuilder sb = new StringBuilder(200);

			if (items.Count > 0)
			{
				sb.Append("<Items>");

				foreach (DescriptionItem item in items)
				{
					sb.Append("<id>");
					sb.Append(item.ID.ToString());
					sb.Append("</id>");
				}

				sb.Append("</Items>");
			}			

			return sb.ToString();
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Gets the path to EPR print templates. Used by Report and Gist object's get-PDF functions. Not hardcoded into objects because
		/// I want other web apps to be able to pass in the url from their relative perspective.
		/// </summary>
		public static string GetPrintReportTemplateUrl()
		{
			//Trace.Write("resolve url " + Page.ResolveUrl("~/default.aspx")); 
			//Trace.Write("app path " + Request.ApplicationPath);
			//Trace.Write("host " + Request.Url.Host); 

			Uri url = HttpContext.Current.Request.Url;
			string appPath = HttpContext.Current.Request.ApplicationPath;

			StringBuilder sb = new StringBuilder(200);

			sb.Append("http://");
			sb.Append(url.Host);

			if (url.Port != 80) //often a port in VS.NET
				sb.Append(":" + url.Port);

			sb.Append(appPath); //the virtual directory ("epr"), if any (often not when local)

			if (appPath.Length > 1)
				sb.Append("/");

			sb.Append("print/");

			//Trace.Write("templatePath " + sb.ToString()); 

			return sb.ToString();
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
				
		/// <summary>
		/// Gets the alphabetic character for the supplied month.
		/// </summary>
		public static string GetLetterForMonth(int month)
		{
			string returnValue = string.Empty;

			switch (month)
			{
				case 1:
					returnValue = "A";
					break;
				case 2:
					returnValue = "B";
					break;
				case 3:
					returnValue = "C";
					break;
				case 4:
					returnValue = "D";
					break;
				case 5:
					returnValue = "E";
					break;
				case 6:
					returnValue = "F";
					break;
				case 7:
					returnValue = "G";
					break;
				case 8:
					returnValue = "H";
					break;
				case 9:
					returnValue = "I";
					break;
				case 10:
					returnValue = "J";
					break;
				case 11:
					returnValue = "K";
					break;
				case 12:
					returnValue = "L";
					break;
			}

			return returnValue;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	}
}