using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ReportHq.Common;
using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports
{
	/// <summary>
	/// A Notification is a simple short message sent in real-time to users of the system. Notifications are retained so off-line users can see them when logged in, as well as review past notifications.
	/// 
	/// TODO:	currently both Report notifications (one message, one recipient) and global notifications (one message, multiple recipients) are in one table, Notifications. for multiple recipient messages,
	///			every message is inserted into the single table for every single recipient. Thought this would be simpler but its actually less ideal than storing in relational tables, because a global 
	///			notification has many columns of info that gets repeated across the row for every user its sent to...lots of duplicate data. should probably go back to using a second delivery-table for 
	///			multiple-recipient notifications, which only has forign keys for the User, and the NotificationID.
	/// 
	/// 
	/// </summary>
	public class Notification
	{
		#region Enums

		public enum Types
		{
			Report = 1,
			Bulletin = 2,
			APB  = 3,
			System = 4
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Properties

		public int ID { get; set; }
		public int TypeID { get; set; }
		//public string Type { get; set; }
		public int ObjectID { get; set; }
		public string Message { get; set; }
		public string Url { get; set; }
		public string Recipient { get; set; }
		public bool Viewed { get; set; }
		//public DateTime CreatedDate { get; set; }

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Constructors

		/// <summary>
		/// Create a notification for a specific recipient.
		/// </summary>
		public Notification(Types type, int objectID, string message, string url, string recipient)
		{
			this.TypeID = (int)type;
			this.ObjectID = objectID; // 66
			this.Message = message; // "Child kidnapped"
			this.Url = url;  // "javascript:openBulletin(66);" or "/event/BasicInfo.aspx?rid=66"
			this.Recipient = recipient;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		/// <summary>
		/// Create a notification for everyone.
		/// </summary>
		public Notification(Types type, int objectID, string message, string url)
		{
			this.TypeID = (int)type;
			this.ObjectID = objectID; // 66
			this.Message = message; // "Child kidnapped"
			this.Url = url;  // "javascript:openBulletin(66);" or "/event/BasicInfo.aspx?rid=66"
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		/// <summary>
		/// Use this when filling a new Notification from datarow. (ex: record from db)
		/// </summary>
		public Notification(DataRow row)
		{
			FillObject(row);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		/// <summary>
		/// Sends the Notification to a single recepient.
		/// </summary>
		/// <param name="username"></param>
		public void Send(string username)
		{
			StatusInfo status = NotificationsDA.InsertNotification(this.TypeID, this.ObjectID, this.Message, this.Url, this.Recipient, username);

			////TODO: if fails, send email to admin
			//if (!status.Success)
			//{

			//}
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		/// <summary>
		/// Sends the Notification to all users in the Users table.
		/// </summary>
		/// <param name="username"></param>
		internal void SendGlobal(string username)
		{
			StatusInfo status = NotificationsDA.InsertGlobalNotification(this.TypeID, this.ObjectID, this.Message, this.Url, username);

			////TODO: if fails, send email to admin
			//if (!status.Success)
			//{

			//}
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Static Methods

		/// <summary>
		/// Gets a collection of new (unread) Notifications. To be used by the NotificationsController for web services.
		/// </summary>
		/// <param name="username">For who.</param>
		/// <param name="type">Report, Bulletin, APB, System, etc..</param>
		/// <param name="includeNewish">If true, db will return notifications less-than 24 hours</param>
		/// <returns></returns>
		public static List<Notification> GetNewNotifications(string username, Types type, bool includeNewish)
		{
			List<Notification> notifications = new List<Notification>();

			DataTable dt = new DataTable();

			//notifications are either for one person in Notifications, or for multiple people, in NotificationGlobalRecipients
			switch (type)
			{
				case Types.APB : //apbs are a global type
					dt = NotificationsDA.GetNewGlobalNotificationsByType(username, (int)type, includeNewish);
					break;

				case Types.Report :
					dt = NotificationsDA.GetNewNotificationsByType(username, (int)type, includeNewish);
					break;
			}
			
			foreach (DataRow row in dt.Rows)
			{
				notifications.Add(new Notification(row));
			}

			return notifications;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Marks all of the supplied Notification IDs as read in the db w/ current timestamp.
		/// </summary>
		/// <param name="username">For whom to mark-as-read.</param>
		/// <param name="itemIDs">The list of items to mark.</param>
		/// <param name="mode">Whether the Notifications are normal 1:1 messages, or global messages.</param>
		/// <returns></returns>
		public static StatusInfo MarkAsRead(string username, string itemIDs, int mode)
		{
			//convert take in command-delimited itemIDs, convert to XML for db proc

			//3,5,6,15

			string[] items = itemIDs.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

			StringBuilder sb = new StringBuilder(100);

			sb.Append("<Items>");

			foreach (string item in items)
			{
				sb.Append(String.Format("<id>{0}</id>", item));
			}

			sb.Append("</Items>");

			//<Items><id>3</id><id>6</id><id>15</id></Items>

			return NotificationsDA.SetNotificationAsRead(username, sb.ToString(), mode);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Gets all non-archived Notifications for user.
		/// </summary>
		/// <param name="username"></param>
		/// <returns></returns>
		public static DataSet GetNotifications(string username)
		{
			return NotificationsDA.GetNotificationsByRecipient(username);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		/// <summary>
		/// Fills this object for a specific GlobalNotification, via constructor.
		/// </summary>
		private void FillObject(DataRow row)
		{
			//required
			this.ID = (int)row["ID"];
			//notification.Type = (string)row["TypeDescription"];
			this.Message = (string)row["Message"];
			this.Url = (string)row["Url"];

			if (row["RelatedObjectID"] != DBNull.Value)
				this.ObjectID = (int)row["RelatedObjectID"];

			if (row["ViewedDate"] != DBNull.Value)
				this.Viewed = true;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion		
	}
}
