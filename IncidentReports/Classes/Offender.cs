using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ReportHq.Common;
using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports
{
	public class Offender
	{
		#region Properties
		
		private int _ID;
		private int _reportID;
		private int _offenderNumber;
		private int _statusTypeID;
		private string _status;
		private string _lastName;
		private string _firstName;
		private string _nickname;
		private int _raceTypeID;
		private string _race;
		private int _genderTypeID;
		private string _gender;
		private DateTime _dateOfBirth;
		private int _height;
		private int _weight;
		private string _streetAddress;
		private string _city;
		private string _state;
		private int _zipCode;
		private string _socialSecurityNumber;
		private string _driversLicenseNumber;
		private int _sobrietyTypeID;
		private string _sobriety;
		private int _injuryTypeID;
		private string _injury;
		private int _treatedTypeID;
		private string _treated;
		private int _arrestTypeID;
		private string _arrestType;
		private string _arrestLocation;
		private DateTime _arrestDateTime;
		private string _arrestCredit;
		private string _transportedBy;
		private string _transportUnit;
		private int _rightsWaivedFormNumber;
		private int _residentTypeID;
		private string _residentType;
		private int _juvenileDispositionTypeID;
		private string _juvenileDisposition;
		private int _district;
		private string _zone;
		private string _subZone;
		private string _additionalDescription;
		private List<OffenderCharge> _charges;

		
		public int ID
		{
			set { _ID = value; }
			get { return _ID; }
		}

		public int ReportID
		{
			set { _reportID = value; }
			get { return _reportID; }
		}

		public int OffenderNumber
		{
			set { _offenderNumber = value; }
			get { return _offenderNumber; }
		}

		public int StatusTypeID
		{
			set { _statusTypeID = value; }
			get { return _statusTypeID; }
		}

		public string Status
		{
			set { _status = value; }
			get { return _status; }
		}

		public string LastName
		{
			set { _lastName = value; }
			get { return _lastName; }
		}

		public string FirstName
		{
			set { _firstName = value; }
			get { return _firstName; }
		}

		public string Nickname
		{
			set { _nickname = value; }
			get { return _nickname; }
		}

		public int RaceTypeID
		{
			set { _raceTypeID = value; }
			get { return _raceTypeID; }
		}

		public string Race
		{
			set { _race = value; }
			get { return _race; }
		}

		public int GenderTypeID
		{
			set { _genderTypeID = value; }
			get { return _genderTypeID; }
		}

		public string Gender
		{
			set { _gender = value; }
			get { return _gender; }
		}

		public DateTime DateOfBirth
		{
			set { _dateOfBirth = value; }
			get { return _dateOfBirth; }
		}

		public int Height
		{
			set { _height = value; }
			get { return _height; }
		}

		public int Weight
		{
			set { _weight = value; }
			get { return _weight; }
		}

		public string StreetAddress
		{
			set { _streetAddress = value; }
			get { return _streetAddress; }
		}

		public string City
		{
			set { _city = value; }
			get { return _city; }
		}

		public string State
		{
			set { _state = value; }
			get { return _state; }
		}

		public int ZipCode
		{
			set { _zipCode = value; }
			get { return _zipCode; }
		}

		public string SocialSecurityNumber
		{
			set { _socialSecurityNumber = value; }
			get { return _socialSecurityNumber; }
		}

		public string DriversLicenseNumber
		{
			set { _driversLicenseNumber = value; }
			get { return _driversLicenseNumber; }
		}

		public int SobrietyTypeID
		{
			set { _sobrietyTypeID = value; }
			get { return _sobrietyTypeID; }
		}

		public string Sobriety
		{
			set { _sobriety = value; }
			get { return _sobriety; }
		}

		public int InjuryTypeID
		{
			set { _injuryTypeID = value; }
			get { return _injuryTypeID; }
		}

		public string Injury
		{
			set { _injury = value; }
			get { return _injury; }
		}

		public int TreatedTypeID
		{
			set { _treatedTypeID = value; }
			get { return _treatedTypeID; }
		}

		public string Treated
		{
			set { _treated = value; }
			get { return _treated; }
		}

		public int ArrestTypeID
		{
			set { _arrestTypeID = value; }
			get { return _arrestTypeID; }
		}

		public string ArrestType
		{
			set { _arrestType = value; }
			get { return _arrestType; }
		}

		public string ArrestLocation
		{
			set { _arrestLocation = value; }
			get { return _arrestLocation; }
		}

		public DateTime ArrestDateTime
		{
			set { _arrestDateTime = value; }
			get { return _arrestDateTime; }
		}

		public string ArrestCredit
		{
			set { _arrestCredit = value; }
			get { return _arrestCredit; }
		}

		public string TransportedBy
		{
			set { _transportedBy = value; }
			get { return _transportedBy; }
		}

		public string TransportUnit
		{
			set { _transportUnit = value; }
			get { return _transportUnit; }
		}

		public int RightsWaivedFormNumber
		{
			set { _rightsWaivedFormNumber = value; }
			get { return _rightsWaivedFormNumber; }
		}

		public int ResidentTypeID
		{
			set { _residentTypeID = value; }
			get { return _residentTypeID; }
		}

		public string ResidentType
		{
			set { _residentType = value; }
			get { return _residentType; }
		}

		public int JuvenileDispositionTypeID
		{
			set { _juvenileDispositionTypeID = value; }
			get { return _juvenileDispositionTypeID; }
		}

		public string JuvenileDisposition
		{
			set { _juvenileDisposition = value; }
			get { return _juvenileDisposition; }
		}

		public int District
		{
			get { return _district; }
			set { _district = value; }
		}

		public string Zone
		{
			get { return _zone; }
			set { _zone = value; }
		}

		public string SubZone
		{
			get { return _subZone; }
			set { _subZone = value; }
		}

		public string AdditionalDescription
		{
			get { return _additionalDescription; }
			set { _additionalDescription = value; }
		}

		public List<OffenderCharge> Charges
		{
			get { return _charges; }
			set { _charges = value; }
		}

		#region DESCRIPTIONS

		//used when filling a complete Offender object.
		private List<DescriptionItem> _descriptionBuildItems;
		private List<DescriptionItem> _descriptionOddityItems;
		private List<DescriptionItem> _descriptionScarItems;
		private List<DescriptionItem> _descriptionTattooItems;
		private List<DescriptionItem> _descriptionApparelItems;
		private List<DescriptionItem> _descriptionSpeechItems;
		private List<DescriptionItem> _descriptionAccentItems;
		private List<DescriptionItem> _descriptionFacialOddityItems;
		private List<DescriptionItem> _descriptionEyeItems;
		private List<DescriptionItem> _descriptionNoseItems;
		private List<DescriptionItem> _descriptionTeethItems;
		private List<DescriptionItem> _descriptionHairColorItems;
		private List<DescriptionItem> _descriptionHairStyleItems;
		private List<DescriptionItem> _descriptionFacialHairItems;
		private List<DescriptionItem> _descriptionComplexionItems;

		public List<DescriptionItem> DescriptionBuildItems
		{
			get
			{
				if (_descriptionBuildItems == null)
					return GetOffenderDescriptorsByCategory(1);
				else
					return _descriptionBuildItems;
			}
			set { _descriptionBuildItems = value; }
		}

		public List<DescriptionItem> DescriptionOddityItems
		{
			get
			{
				if (_descriptionOddityItems == null)
					return GetOffenderDescriptorsByCategory(2);
				else
					return _descriptionOddityItems;
			}
			set { _descriptionOddityItems = value; }
		}

		public List<DescriptionItem> DescriptionScarItems
		{
			get
			{
				if (_descriptionScarItems == null)
					return GetOffenderDescriptorsByCategory(3);
				else
					return _descriptionScarItems;
			}
			set { _descriptionScarItems = value; }
		}


		public List<DescriptionItem> DescriptionTattooItems
		{
			get
			{
				if (_descriptionTattooItems == null)
					return GetOffenderDescriptorsByCategory(4);
				else
					return _descriptionTattooItems;
			}
			set { _descriptionTattooItems = value; }
		}

		public List<DescriptionItem> DescriptionApparelItems
		{
			get
			{
				if (_descriptionApparelItems == null)
					return GetOffenderDescriptorsByCategory(5);
				else
					return _descriptionApparelItems;
			}
			set { _descriptionApparelItems = value; }
		}

		public List<DescriptionItem> DescriptionSpeechItems
		{
			get
			{
				if (_descriptionSpeechItems == null)
					return GetOffenderDescriptorsByCategory(6);
				else
					return _descriptionSpeechItems;
			}
			set { _descriptionSpeechItems = value; }
		}

		public List<DescriptionItem> DescriptionAccentItems
		{
			get
			{
				if (_descriptionAccentItems == null)
					return GetOffenderDescriptorsByCategory(7);
				else
					return _descriptionAccentItems;
			}
			set { _descriptionAccentItems = value; }
		}

		public List<DescriptionItem> DescriptionFacialOddityItems
		{
			get
			{
				if (_descriptionFacialOddityItems == null)
					return GetOffenderDescriptorsByCategory(8);
				else
					return _descriptionFacialOddityItems;
			}
			set { _descriptionFacialOddityItems = value; }
		}

		public List<DescriptionItem> DescriptionEyeItems
		{
			get
			{
				if (_descriptionEyeItems == null)
					return GetOffenderDescriptorsByCategory(9);
				else
					return _descriptionEyeItems;
			}
			set { _descriptionEyeItems = value; }
		}

		public List<DescriptionItem> DescriptionNoseItems
		{
			get
			{
				if (_descriptionNoseItems == null)
					return GetOffenderDescriptorsByCategory(10);
				else
					return _descriptionNoseItems;
			}
			set { _descriptionNoseItems = value; }
		}

		public List<DescriptionItem> DescriptionTeethItems
		{
			get
			{
				if (_descriptionTeethItems == null)
					return GetOffenderDescriptorsByCategory(11);
				else
					return _descriptionTeethItems;
			}
			set { _descriptionTeethItems = value; }
		}

		public List<DescriptionItem> DescriptionHairColorItems
		{
			get
			{
				if (_descriptionHairColorItems == null)
					return GetOffenderDescriptorsByCategory(12);
				else
					return _descriptionHairColorItems;
			}
			set { _descriptionHairColorItems = value; }
		}

		public List<DescriptionItem> DescriptionHairStyleItems
		{
			get
			{
				if (_descriptionHairStyleItems == null)
					return GetOffenderDescriptorsByCategory(13);
				else
					return _descriptionHairStyleItems;
			}
			set { _descriptionHairStyleItems = value; }
		}

		public List<DescriptionItem> DescriptionFacialHairItems
		{
			get
			{
				if (_descriptionFacialHairItems == null)
					return GetOffenderDescriptorsByCategory(14);
				else
					return _descriptionFacialHairItems;
			}
			set { _descriptionFacialHairItems = value; }
		}

		public List<DescriptionItem> DescriptionComplexionItems
		{
			get
			{
				if (_descriptionComplexionItems == null)
					return GetOffenderDescriptorsByCategory(15);
				else
					return _descriptionComplexionItems;
			}
			set { _descriptionComplexionItems = value; }
		}



		#endregion

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Constructors

		/// <summary>
		/// Use this when building an empty or new Offender.
		/// </summary>
		public Offender()
		{

		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Use this when retrieving an existing Offender.
		/// </summary>
		public Offender(int offenderID)
		{
			DataSet ds = OffenderDA.GetOffenderByID(offenderID);

			if (ds.Tables[0].Rows.Count > 0)
				FillObject(this, ds.Tables[0].Rows[0]);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Use this when filling a new Offender from values collected from a Grid. (ex: an insert operation)
		/// </summary>
		public Offender(Hashtable newValues)
		{
			this.OffenderNumber = (int)newValues["OffenderNumber"];
			this.StatusTypeID = (int)newValues["OffenderStatusTypeID"];
			this.LastName = (string)newValues["LastName"];
			this.FirstName = (string)newValues["FirstName"];
			this.Nickname = (string)newValues["Nickname"];
			this.RaceTypeID = (int)newValues["RaceTypeID"];
			this.GenderTypeID = (int)newValues["GenderTypeID"];

			if (newValues["DateOfBirth"] != null)
				this.DateOfBirth = (DateTime)newValues["DateOfBirth"];

			this.Height = (int)newValues["Height"];
			this.Weight = (int)newValues["Weight"];
			this.StreetAddress = (string)newValues["StreetAddress"];
			this.City = (string)newValues["City"];
			this.State = (string)newValues["State"];
			this.ZipCode = (int)newValues["ZipCode"];
			this.SocialSecurityNumber = (string)newValues["SocialSecurityNumber"];
			this.DriversLicenseNumber = (string)newValues["DriversLicenseNumber"];
			this.SobrietyTypeID = (int)newValues["SobrietyTypeID"];
			this.InjuryTypeID = (int)newValues["InjuryTypeID"];
			this.TreatedTypeID = (int)newValues["TreatedTypeID"];
			this.ArrestTypeID = (int)newValues["ArrestTypeID"];
			this.ArrestLocation = (string)newValues["ArrestLocation"];
			this.ArrestDateTime = (DateTime)newValues["ArrestDateTime"];
			this.ArrestCredit = (string)newValues["ArrestCredit"];
			this.TransportedBy = (string)newValues["TransportedBy"];
			this.TransportUnit = (string)newValues["TransportUnit"];
			this.RightsWaivedFormNumber = (int)newValues["RightsWaivedFormNumber"];
			this.ResidentTypeID = (int)newValues["ResidentTypeID"];
			this.JuvenileDispositionTypeID = (int)newValues["JuvenileDispositionTypeID"];
			this.District = (int)newValues["District"];
			this.Zone = (string)newValues["Zone"];
			this.SubZone = (string)newValues["SubZone"];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		/// <summary>
		/// Use this when filling a new Offender from datarow. (ex: record from db)
		/// </summary>
		public Offender(DataRow row, bool fillCollections)
		{
			FillObject(this, row);

			if (fillCollections)
				FillCollections(this);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Completely filling an Offender object w/ its respective data.
		/// </summary>
		private void FillCollections(Offender offender)
		{
			//charges
			List<OffenderCharge> charges = new List<OffenderCharge>();

			DataTable table = OffenderCharge.GetOffenderChargesByOffender(offender.ID);
			foreach (DataRow row in table.Rows)
				charges.Add(new OffenderCharge(row));

			offender.Charges = charges;

			//offender-description collections are auto-filled on first property requests (ex: offender.DescriptionBuildItems)
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		public StatusInfo Add(string createdBy)
		{
			return OffenderDA.InsertOffender(this.ReportID,
											 this.OffenderNumber,
											 this.StatusTypeID,
											 this.LastName,
											 this.FirstName,
											 this.Nickname,
											 this.RaceTypeID,
											 this.GenderTypeID,
											 this.DateOfBirth,
											 this.Height,
											 this.Weight,
											 this.StreetAddress,
											 this.City,
											 this.State,
											 this.ZipCode,
											 this.SocialSecurityNumber,
											 this.DriversLicenseNumber,
											 this.SobrietyTypeID,
											 this.InjuryTypeID,
											 this.TreatedTypeID,											 
											 this.ArrestTypeID,
											 this.ArrestLocation,
											 this.ArrestDateTime,
											 this.ArrestCredit,
											 this.TransportedBy,
											 this.TransportUnit,
											 this.RightsWaivedFormNumber,
											 this.ResidentTypeID,
											 this.JuvenileDispositionTypeID,
											 this.District,
											 this.Zone,
											 this.SubZone,
											 createdBy);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public StatusInfo Update(string modifiedBy)
		{
			return OffenderDA.UpdateOffender(this.ID,
											 this.OffenderNumber,
											 this.StatusTypeID,
											 this.LastName,
											 this.FirstName,
											 this.Nickname,
											 this.RaceTypeID,
											 this.GenderTypeID,
											 this.DateOfBirth,
											 this.Height,
											 this.Weight,
											 this.StreetAddress,
											 this.City,
											 this.State,
											 this.ZipCode,
											 this.SocialSecurityNumber,
											 this.DriversLicenseNumber,
											 this.SobrietyTypeID,
											 this.InjuryTypeID,
											 this.TreatedTypeID,
											 this.ArrestTypeID,
											 this.ArrestLocation,
											 this.ArrestDateTime,
											 this.ArrestCredit,
											 this.TransportedBy,
											 this.TransportUnit,
											 this.RightsWaivedFormNumber,
											 this.ResidentTypeID,
											 this.JuvenileDispositionTypeID,
											 this.District,
											 this.Zone,
											 this.SubZone,
											 modifiedBy);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo DeleteOffender(int offenderID)
		{
			return OffenderDA.DeleteOffender(offenderID);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public StatusInfo UpdateOffenderDescription(string modifiedBy)
		{
			//merge all the Description items into one list
			List<DescriptionItem> items = this.DescriptionBuildItems;
			items.AddRange(this.DescriptionOddityItems);
			items.AddRange(this.DescriptionScarItems);
			items.AddRange(this.DescriptionTattooItems);
			items.AddRange(this.DescriptionApparelItems);
			items.AddRange(this.DescriptionSpeechItems);
			items.AddRange(this.DescriptionAccentItems);
			items.AddRange(this.DescriptionFacialOddityItems);
			items.AddRange(this.DescriptionEyeItems);
			items.AddRange(this.DescriptionNoseItems);
			items.AddRange(this.DescriptionTeethItems);
			items.AddRange(this.DescriptionHairColorItems);
			items.AddRange(this.DescriptionHairStyleItems);
			items.AddRange(this.DescriptionFacialHairItems);
			items.AddRange(this.DescriptionComplexionItems);

			//then produce an xmlstring of 'em ("<Items><id>3</id><id>6</id><id>15</id></Items>"), which is used for the INSERT.
			string itemIdsXml = Common.GetXmlStringOfIds(items);

			//TODO: TEST: that the new SQL code used by this proc for returning a custom error message -- RAISERROR (50002, 16, 1) -- works. try to break it to trigger the error.. otherwise decide if custom error is worth keeping.
						
			return OffenderDA.UpdateOffenderDescriptors(this.ID, itemIdsXml, this.AdditionalDescription, modifiedBy);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Static Methods

		/// <summary>
		/// Gets a DataTable of a Report's Offenders.
		/// </summary>
		public static DataTable GetOffendersByReport(int reportID)
		{
			return OffenderDA.GetOffendersByReport(reportID);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Find Offenders using optional search criteria.
		public static DataTable GetOffendersByCriteria(string firstName,
													   string lastName,
													   string nickname,
													   string driversLicenseNumber,
													   string socialSecurityNumber,
													   int raceTypeID,
													   int genderTypeID,
													   int height,
													   int weight,
													   string descriptorsXml)
		{
			return OffenderDA.GetOffendersByCriteria(firstName,
												     lastName,
													 nickname,
												     driversLicenseNumber,
												     socialSecurityNumber,
												     raceTypeID,
												     genderTypeID,
												     height,
												     weight,
													 descriptorsXml);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo AddDomesticViolenceOffender(int reportID, int offenderID, Hashtable newValues, string createdBy)
		{
			return OffenderDA.InsertDomesticViolenceOffender(reportID,
															 offenderID,
															 (int)newValues["MedicalTreatmentTypeID"],
															 (string)newValues["MedicalUnitNumber"],
															 (string)newValues["MedicalAddress"],
															 (bool)newValues["MadeComments"],
															 (string)newValues["Comments"],
															 Common.GetXmlStringOfIds((List<DescriptionItem>)newValues["Descriptors"]),
															 Common.GetXmlStringOfIds((List<DescriptionItem>)newValues["Injuries"]),
															 createdBy);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo UpdateDomesticViolenceOffender(int offenderID, Hashtable newValues, string modifiedBy)
		{
			return OffenderDA.UpdateDomesticViolenceOffender(offenderID,
															 (int)newValues["MedicalTreatmentTypeID"],
															 (string)newValues["MedicalUnitNumber"],
															 (string)newValues["MedicalAddress"],
															 (bool)newValues["MadeComments"],
															 (string)newValues["Comments"],
															 Common.GetXmlStringOfIds((List<DescriptionItem>)newValues["Descriptors"]),
															 Common.GetXmlStringOfIds((List<DescriptionItem>)newValues["Injuries"]),
															 modifiedBy);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo DeleteDomesticViolenceOffender(int offenderID)
		{
			return OffenderDA.DeleteDomesticViolenceOffender(offenderID);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetStatusTypes()
		{
			const string KEYNAME = "dtOffenderStatusTypes";

			Cache cache = HttpRuntime.Cache;
			
			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = OffenderDA.GetOffenderStatusTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetArrestTypes()
		{
			const string KEYNAME = "dtArrestTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = OffenderDA.GetArrestTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetJuvenileDispositionTypes()
		{
			const string KEYNAME = "dtJuvenileDispositionTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = OffenderDA.GetJuvenileDispositionTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetOffenderBuildTypes()
		{
			const string KEYNAME = "dtOffenderBuildTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = OffenderDA.GetOffenderDescriptorTypesByCategory(1);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetOffenderOddityTypes()
		{
			const string KEYNAME = "dtOffenderOddityTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = OffenderDA.GetOffenderDescriptorTypesByCategory(2);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetOffenderScarTypes()
		{
			const string KEYNAME = "dtOffenderScarTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = OffenderDA.GetOffenderDescriptorTypesByCategory(3);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetOffenderTattooTypes()
		{
			const string KEYNAME = "dtOffenderTattooTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = OffenderDA.GetOffenderDescriptorTypesByCategory(4);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetOffenderApparelTypes()
		{
			const string KEYNAME = "dtOffenderApparelTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = OffenderDA.GetOffenderDescriptorTypesByCategory(5);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetOffenderSpeechTypes()
		{
			const string KEYNAME = "dtOffenderSpeechTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = OffenderDA.GetOffenderDescriptorTypesByCategory(6);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetOffenderAccentTypes()
		{
			const string KEYNAME = "dtOffenderAccentTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = OffenderDA.GetOffenderDescriptorTypesByCategory(7);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetOffenderFacialOddityTypes()
		{
			const string KEYNAME = "dtOffenderFacialOddityTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = OffenderDA.GetOffenderDescriptorTypesByCategory(8);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetOffenderEyeTypes()
		{
			const string KEYNAME = "dtOffenderEyeTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = OffenderDA.GetOffenderDescriptorTypesByCategory(9);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetOffenderNoseTypes()
		{
			const string KEYNAME = "dtOffenderNoseTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = OffenderDA.GetOffenderDescriptorTypesByCategory(10);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetOffenderTeethTypes()
		{
			const string KEYNAME = "dtOffenderTeethTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = OffenderDA.GetOffenderDescriptorTypesByCategory(11);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetOffenderHairColorTypes()
		{
			const string KEYNAME = "dtOffenderHairColorTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = OffenderDA.GetOffenderDescriptorTypesByCategory(12);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetOffenderHairStyleTypes()
		{
			const string KEYNAME = "dtOffenderHairStyleTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = OffenderDA.GetOffenderDescriptorTypesByCategory(13);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetOffenderFacialHairTypes()
		{
			const string KEYNAME = "dtOffenderFacialHairTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = OffenderDA.GetOffenderDescriptorTypesByCategory(14);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetOffenderComplexionTypes()
		{
			const string KEYNAME = "dtOffenderComplexionTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = OffenderDA.GetOffenderDescriptorTypesByCategory(15);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Gets a DataTable of a Report's Domestic Violence Offenders.
		/// </summary>
		public static DataTable GetDomesticViolenceOffendersByReport(int reportID)
		{
			return OffenderDA.GetDomesticViolenceOffendersByReport(reportID);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Gets a Domestic Violence Offender's descriptors.
		/// </summary>
		public static List<DescriptionItem> GetDomesticViolenceDescriptorsByOffender(int offenderID)
		{
			List<DescriptionItem> items = new List<DescriptionItem>();

			DescriptionItem newItem;
			foreach (DataRow row in OffenderDA.GetDomesticViolenceDescriptorsByOffender(offenderID).Rows)
			{
				newItem = new DescriptionItem();
				newItem.ID = (int)row["DescriptorTypeID"];
				newItem.Description = (string)row["Description"];

				items.Add(newItem);
			}

			return items;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Gets a Domestic Violence Victim's injuries.
		/// </summary>
		public static List<DescriptionItem> GetDomesticViolenceOffenderInjuries(int offenderID)
		{
			List<DescriptionItem> items = new List<DescriptionItem>();

			DescriptionItem newItem;
			foreach (DataRow row in OffenderDA.GetDomesticViolenceOffenderInjuries(offenderID).Rows)
			{
				newItem = new DescriptionItem();
				newItem.ID = (int)row["InjuryTypeID"];
				newItem.Description = (string)row["Description"];

				items.Add(newItem);
			}

			return items;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		/// <summary>
		/// Filling this object for a specific Offender, via constructor.
		/// </summary>
		private void FillObject(Offender offender, DataRow row)
		{
			//required
			offender.ID = (int)row["OffenderID"];
			offender.ReportID = (int)row["ReportID"];
			offender.OffenderNumber = (int)row["OffenderNumber"];
			offender.StatusTypeID = (int)row["OffenderStatusTypeID"];
			offender.Status = (string)row["Status"];

			if (row["LastName"] != DBNull.Value)
				offender.LastName = (string)row["LastName"];

			if (row["FirstName"] != DBNull.Value)
				offender.FirstName = (string)row["FirstName"];
			
			if (row["Nickname"] != DBNull.Value)
				offender.Nickname = (string)row["Nickname"];

			if (row["RaceTypeID"] != DBNull.Value)
				offender.RaceTypeID = (int)row["RaceTypeID"];

			if (row["Race"] != DBNull.Value)
				offender.Race = (string)row["Race"];

			if (row["GenderTypeID"] != DBNull.Value)
				offender.GenderTypeID = (int)row["GenderTypeID"];

			if (row["Gender"] != DBNull.Value)
				offender.Gender = (string)row["Gender"];

			if (row["DateOfBirth"] != DBNull.Value)
				offender.DateOfBirth = (DateTime)row["DateOfBirth"];

			if (row["Height"] != DBNull.Value)
				offender.Height = (int)row["Height"];

			if (row["Weight"] != DBNull.Value)
				offender.Weight = (int)row["Weight"];

			if (row["StreetAddress"] != DBNull.Value)
				offender.StreetAddress = (string)row["StreetAddress"];

			if (row["City"] != DBNull.Value)
				offender.City = (string)row["City"];

			if (row["State"] != DBNull.Value)
				offender.State = (string)row["State"];

			if (row["ZipCode"] != DBNull.Value)
				offender.ZipCode = (int)row["ZipCode"];

			if (row["SocialSecurityNumber"] != DBNull.Value)
				offender.SocialSecurityNumber = (string)row["SocialSecurityNumber"];

			if (row["DriversLicenseNumber"] != DBNull.Value)
				offender.DriversLicenseNumber = (string)row["DriversLicenseNumber"];

			if (row["SobrietyTypeID"] != DBNull.Value)
				offender.SobrietyTypeID = (int)row["SobrietyTypeID"];

			if (row["Sobriety"] != DBNull.Value)
				offender.Sobriety = (string)row["Sobriety"];

			if (row["InjuryTypeID"] != DBNull.Value)
				offender.InjuryTypeID = (int)row["InjuryTypeID"];

			if (row["Injury"] != DBNull.Value)
				offender.Injury = (string)row["Injury"];

			if (row["TreatedTypeID"] != DBNull.Value)
				offender.TreatedTypeID = (int)row["TreatedTypeID"];

			if (row["Treated"] != DBNull.Value)
				offender.Treated = (string)row["Treated"];

			if (row["ArrestTypeID"] != DBNull.Value)
				offender.ArrestTypeID = (int)row["ArrestTypeID"];

			if (row["ArrestType"] != DBNull.Value)
				offender.ArrestType = (string)row["ArrestType"];

			if (row["ArrestLocation"] != DBNull.Value)
				offender.ArrestLocation = (string)row["ArrestLocation"];

			if (row["ArrestDate"] != DBNull.Value)
				offender.ArrestDateTime = (DateTime)row["ArrestDate"];

			if (row["ArrestCredit"] != DBNull.Value)
				offender.ArrestCredit = (string)row["ArrestCredit"];

			if (row["TransportedBy"] != DBNull.Value)
				offender.TransportedBy = (string)row["TransportedBy"];

			if (row["TransportUnit"] != DBNull.Value)
				offender.TransportUnit = (string)row["TransportUnit"];

			if (row["RightsWaivedFormNumber"] != DBNull.Value)
				offender.RightsWaivedFormNumber = (int)row["RightsWaivedFormNumber"];

			if (row["ResidentTypeID"] != DBNull.Value)
				offender.ResidentTypeID = (int)row["ResidentTypeID"];

			if (row["ResidentType"] != DBNull.Value)
				offender.ResidentType = (string)row["ResidentType"];

			if (row["JuvenileDispositionTypeID"] != DBNull.Value)
				offender.JuvenileDispositionTypeID = (int)row["JuvenileDispositionTypeID"];

			if (row["JuvenileDisposition"] != DBNull.Value)
				offender.JuvenileDisposition = (string)row["JuvenileDisposition"];

			if (row["District"] != DBNull.Value)
				offender.District = (int)row["District"];

			if (row["Zone"] != DBNull.Value)
				offender.Zone = (string)row["Zone"];

			if (row["SubZone"] != DBNull.Value)
				offender.SubZone = (string)row["SubZone"];

			if (row["AdditionalDescription"] != DBNull.Value)
				offender.AdditionalDescription = (string)row["AdditionalDescription"];
			
			//boiler plate
			//if (row["CreatedBy"] != DBNull.Value)
			//    offender.CreatedBy = (string)row["CreatedBy"];

			//if (row["CreatedDate"] != DBNull.Value)
			//    offender.CreatedDate = (DateTime)row["CreatedDate"];

			//if (row["LastModifiedBy"] != DBNull.Value)
			//    offender.LastModifiedBy = (string)row["LastModifiedBy"];

			//if (row["LastModifiedDate"] != DBNull.Value)
			//    offender.LastModifiedDate = (DateTime)row["LastModifiedDate"];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		private List<DescriptionItem> GetOffenderDescriptorsByCategory(int categoryID)
		{
			List<DescriptionItem> items = new List<DescriptionItem>();

			DescriptionItem newItem;
			foreach (DataRow row in OffenderDA.GetOffenderDescriptorsByCategory(this.ID, categoryID).Rows)
			{
				newItem = new DescriptionItem();
				newItem.ID = (int)row["OffenderDescriptorTypeID"];
				newItem.Description = (string)row["Description"];

				items.Add(newItem);
			}

			return items;
		
			//1 = Build
			//2	= Oddities
			//3 = Scars
			//4 = Tattoos
			//5 = Apparel
			//6 = Speech
			//7 = Accent
			//8 = Facial Oddities
			//9 = Eyes
			//10 = Nose
			//11 = Teeth
			//12 = Hair Color
			//13 = Hair Style
			//14 = Facial Hair
			//15 = Complexion
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
