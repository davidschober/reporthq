using System;
using System.Data;
using System.Collections;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ReportHq.Common;
using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports
{
	public class PropertyItem : Bases.PropertyEvidenceItem
	{
		#region Constructors

		/// <summary>
		/// Use this when filling a new PropertyEvidence from values collected from a Grid. (ex: an insert operation)
		/// </summary>
		public PropertyItem(Hashtable newValues)
		{
			//CNO.NOPD.EPR.Property.

			this.LossTypeID = (int)newValues["LossTypeID"];
			this.Quantity = Convert.ToDecimal(newValues["Quantity"]);
			this.TypeID = (int)newValues["TypeID"];
			this.NarcoticWeightTypeID = (int)newValues["NarcoticWeightTypeID"];
			this.NarcoticTypeID = (int)newValues["NarcoticTypeID"];
			this.Description = (string)newValues["Description"];
			this.Brand = (string)newValues["Brand"];
			this.SerialNumber = (string)newValues["SerialNumber"];
			this.Value = (int)newValues["Value"];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Use this when filling a new PropertyEvidence from datarow. (ex: record from db)
		/// </summary>
		public PropertyItem(DataRow row)
		{
			FillObject(this, row);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		public StatusInfo AddProperty(string createdBy)
		{
			return PropertyEvidenceDA.InsertProperty(this.ReportID,
													 this.LossTypeID,
													 this.Quantity,
													 this.TypeID,
													 this.NarcoticWeightTypeID,
													 this.NarcoticTypeID,
													 this.Description,
													 this.Brand,
													 this.SerialNumber,
													 this.Value,
													 createdBy);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public StatusInfo UpdateProperty(string modifiedBy)
		{
			return PropertyEvidenceDA.UpdateProperty(this.ID,
													 this.LossTypeID,
													 this.Quantity,
													 this.TypeID,
													 this.NarcoticWeightTypeID,
													 this.NarcoticTypeID,
													 this.Description,
													 this.Brand,
													 this.SerialNumber,
													 this.Value,
													 modifiedBy);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Static Methods

		public static StatusInfo DeleteProperty(int propertyID)
		{
			return PropertyEvidenceDA.DeleteProperty(propertyID);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetPropertyByReport(int reportID)
		{
			return PropertyEvidenceDA.GetPropertyByReport(reportID);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		/// <summary>
		/// Fills this object for a specific VictimPerson, via constructor.
		/// </summary>
		private void FillObject(PropertyItem property, DataRow row)
		{
			//required
			property.ID = (int)row["PropertyID"];
			property.ReportID = (int)row["ReportID"];
			property.LossTypeID = (int)row["PropertyEvidenceLossTypeID"];
			property.LossType = (string)row["LossType"];

			if (row["Quantity"] != DBNull.Value)
				property.Quantity = (decimal)row["Quantity"];

			if (row["PropertyTypeID"] != DBNull.Value)
				property.TypeID = (int)row["PropertyTypeID"];

			if (row["Type"] != DBNull.Value)
				property.Type = (string)row["Type"];

			if (row["NarcoticWeightTypeID"] != DBNull.Value)
				property.NarcoticWeightTypeID = (int)row["NarcoticWeightTypeID"];

			if (row["NarcoticWeightUnit"] != DBNull.Value)
				property.NarcoticWeightUnit = (string)row["NarcoticWeightUnit"];

			if (row["NarcoticTypeID"] != DBNull.Value)
				property.NarcoticTypeID = (int)row["NarcoticTypeID"];

			if (row["NarcoticType"] != DBNull.Value)
				property.NarcoticType = (string)row["NarcoticType"];

			if (row["Description"] != DBNull.Value)
				property.Description = (string)row["Description"];

			if (row["Brand"] != DBNull.Value)
				property.Brand = (string)row["Brand"];

			if (row["SerialNumber"] != DBNull.Value)
				property.SerialNumber = (string)row["SerialNumber"];

			if (row["Value"] != DBNull.Value)
				property.Value = (int)row["Value"];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
