﻿<%@ Page Title="Report HQ - Get Started" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ReportHq.IncidentReports.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="~/fonts/webfonts.css" type="text/css" />
	<link rel="stylesheet" href="~/css/font-awesome.min.css" type="text/css" />
	<link rel="stylesheet" href="~/css/normalPage.css" type="text/css" />
	<link rel="stylesheet" href="~/css/common.css" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	Let's get started:

	<div style="margin-top:50px;">
			
		<a href="YourDashboard.aspx" title="Your Dashboard" class="flatButton big">Your Dashboard</a>
		<a href="/RecordsRoom/" title="Access to archived, final reports" class="flatButton big">Records Room</a>
				
	</div>

</asp:Content>
