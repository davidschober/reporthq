﻿<%@ Page Title="Report HQ - Admin" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="ReportHq.IncidentReports.Event.Admin" %>
<%@ Register TagPrefix="ReportHq" TagName="Admin" src="controls/Admin.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="../css/reportForm.css" type="text/css" />
	<script type="text/javascript" src="../js/radComboBoxHelpers.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<ReportHq:ReportMainNav id="ucReportMainNav" CurrentSection="Event" OnNavItemClicked="ucMajorSections_NavItemClicked" runat="server" />

	<ReportHq:Admin ID="ucAdmin" OnNavItemClicked="ucAdmin_NavItemClicked" runat="server" />

</asp:Content>