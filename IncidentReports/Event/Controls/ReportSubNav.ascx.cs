﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports;

namespace ReportHq.IncidentReports.Event.Controls
{
	public partial class ReportSubNav : System.Web.UI.UserControl
	{
		#region Enums

		public enum NavSections
		{
			BasicInfo,
			Ucr,
			Admin,
			None
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Properties

		private NavSections _currentSection;
		private string _targetRedirectUrl;
		private string _validationGroup;
				
		/// <summary>
		/// Which main section link the user is currently at.
		/// </summary>
		public NavSections CurrentSection
		{
			get { return _currentSection; }
			set { _currentSection = value; }
		}

		/// <summary>
		/// Where to bring the user after clicking; used by the consuming .ASPX/ascx page
		/// </summary>
		public string TargetRedirectUrl
		{
			get { return _targetRedirectUrl; }
			set { _targetRedirectUrl = value; }
		}

		public string ValidationGroup
		{
			get { return _validationGroup; }
			set { _validationGroup = value; }
		}

		
		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		#endregion

		#region Events

		//public delegate for LinkButton nav-item click
		public delegate void NavItemClick(object sender, EventArgs e);

		//public event for LinkButton click; will be handled by the consuming pages
		public event NavItemClick NavItemClicked;

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
		
		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			//set validation group used by page's validator controls
			if (!string.IsNullOrEmpty(_validationGroup))
			{
				lbBasicInfo.ValidationGroup = _validationGroup;
				lbUcr.ValidationGroup = _validationGroup;
				lbAdmin.ValidationGroup = _validationGroup;
			}

			//set selected-item state
			switch (_currentSection)
			{
				case NavSections.BasicInfo:
					lblBasicInfo.Visible = true;
					lbBasicInfo.Visible = false;
					break;

				case NavSections.Ucr:
					lblUcr.Visible = true;
					lbUcr.Visible = false;
					break;
					
				case NavSections.Admin:
					lblAdmin.Visible = true;
					lbAdmin.Visible = false;
					break;
			}
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbBasicInfo_Click(object sender, EventArgs e)
		{
			_targetRedirectUrl = "BasicInfo.aspx";

			//fire event for consuming page to handler
			NavItemClicked(this, e);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbUcr_Click(object sender, EventArgs e)
		{
			_targetRedirectUrl = "Ucr.aspx";

			//fire event for consuming page to handler
			NavItemClicked(this, e);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbAdmin_Click(object sender, EventArgs e)
		{
			_targetRedirectUrl = "Admin.aspx";

			//fire event for consuming page to handler
			NavItemClicked(this, e);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}