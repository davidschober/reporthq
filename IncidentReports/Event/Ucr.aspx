﻿<%@ Page Title="Report HQ - UCR" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="Ucr.aspx.cs" Inherits="ReportHq.IncidentReports.Event.Ucr" %>
<%@ Register TagPrefix="ReportHq" TagName="Ucr" src="controls/Ucr.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="../css/reportForm.css" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<ReportHq:ReportMainNav id="ucReportMainNav" CurrentSection="Event" OnNavItemClicked="ucMajorSections_NavItemClicked" runat="server" />

	<ReportHq:Ucr ID="ucUcr" OnNavItemClicked="ucUcr_NavItemClicked" runat="server" />

</asp:Content>
