﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OffendersActions.ascx.cs" Inherits="ReportHq.IncidentReports.ModusOperandi.Controls.OffendersActions" %>
<%@ Register TagPrefix="ReportHq" TagName="ReportSubNav" src="ReportSubNav.ascx" %>

<ReportHq:ReportSubNav ID="ucReportSubNav" CurrentSection="OffendersActions" OnNavItemClicked="ucReportSubNav_NavItemClicked" runat="server" />

<Telerik:RadFormDecorator id="radFormDecorator" DecoratedControls="CheckBoxes" Skin="Metro" runat="server" />

<div class="formBox" style="width:640px;">

	<table width="100%" border="0" class="bordered">
		<tr>
			<td colspan="2" valign="top" class="bordered">
				<div class="checkBoxListLabel">Offender's Approach to Victim</div>
				<asp:CheckBoxList ID="cblApproaches" DataValueField="OffenderActionTypeID" DataTextField="Description" EnableViewState="false" RepeatColumns="2" CellPadding="0" CellSpacing="0" Width="100%" runat="server" />
			</td>
			<td valign="top" class="bordered">
				<div class="checkBoxListLabel">Impersonated</div>			
				<asp:CheckBoxList ID="cblImpersonationTypes" DataValueField="OffenderActionTypeID" DataTextField="Description" EnableViewState="false" RepeatLayout="Flow" runat="server" />
			</td>
		</tr>
		<tr>
			<td valign="top" class="bordered">
				<div class="checkBoxListLabel">Weapon</div>
				<asp:CheckBoxList ID="cblWeapons" DataValueField="OffenderActionTypeID" DataTextField="Description" EnableViewState="false" RepeatLayout="Flow" runat="server" />
			</td>
			<td valign="top" class="bordered">
				<div class="checkBoxListLabel">Firearm Features</div>
				<asp:CheckBoxList ID="cblFirearmFeatures" DataValueField="OffenderActionTypeID" DataTextField="Description" EnableViewState="false" RepeatLayout="Flow" runat="server" />
			</td>
			<td valign="top" class="bordered">
				<div class="checkBoxListLabel">Property Crimes</div>			
				<asp:CheckBoxList ID="cblPropertyCrimes" DataValueField="OffenderActionTypeID" DataTextField="Description" EnableViewState="false" RepeatLayout="Flow" runat="server" />
			</td>
		</tr>
		<tr>
			<td colspan="2" valign="top" class="bordered">
				<div class="checkBoxListLabel">Person Crime</div>			
				<asp:CheckBoxList ID="cblPersonCrimes" DataValueField="OffenderActionTypeID" DataTextField="Description" EnableViewState="false" RepeatColumns="2" CellPadding="0" CellSpacing="0" Width="100%" runat="server" />
			</td>
			<td valign="top" class="bordered">
				<div class="checkBoxListLabel">Sex Crime Specific</div>
				<asp:CheckBoxList ID="cblSexCrimes" DataValueField="OffenderActionTypeID" DataTextField="Description" EnableViewState="false" RepeatLayout="Flow" runat="server" />
			</td>
		</tr>
	</table><br />
	
	<div class="label">"Other" Info:</div>
	<asp:TextBox ID="tbOffendersActionsDesc" TextMode="MultiLine" Rows="5" Columns="50" EnableViewState="False" MaxLength="500" onkeypress="return isMaxLength(this, 500);" onblur="trimSize(this, 500);" runat="server"/><br />
	
</div>