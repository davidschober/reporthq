﻿<%@ Page Title="Report HQ - Crime Location" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="CrimeLocation.aspx.cs" Inherits="ReportHq.IncidentReports.ModusOperandi.CrimeLocation" %>
<%@ Register TagPrefix="ReportHq" TagName="CrimeLocation" src="controls/CrimeLocation.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="../css/reportForm.css" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<ReportHq:ReportMainNav id="ucReportMainNav" CurrentSection="ModusOperandi" OnNavItemClicked="ucMajorSections_NavItemClicked" runat="server" />
	
	<ReportHq:CrimeLocation ID="ucCrimeLocation" OnNavItemClicked="ucCrimeLocation_NavItemClicked" runat="server" />

</asp:Content>
