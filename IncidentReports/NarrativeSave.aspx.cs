﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Controls;

namespace ReportHq.IncidentReports
{
	public partial class NarrativeSave : Page
	{
		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			//get report from form-supplied ReportID
			Report report = new Report(Int32.Parse(Request.Form["rid"]));

			//set narrative from form-supplied text
			report.Narrative = Request.Form["narrative"];

			//save it
			StatusInfo success = report.UpdateNarrative(this.User.Identity.Name);
			
			//VILEMOBILE\Administrator

			//if (success.Success)
			//{
				//update the session copy of this report
				//Session["theReport"] = report; //may not want to...if this page is running in the background and user has another report in the foreground, dont want to dirty session copy.

				//when working w/ a page that inherits from ReportPage base. but i can get away with cheating here by hardcoding the session variable keyname. a hack, but less overhead.
				//if (status.Success == true)
				//	base.Report = report;  
			//}
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion	
	}
}