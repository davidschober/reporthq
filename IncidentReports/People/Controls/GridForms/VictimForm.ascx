﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VictimForm.ascx.cs" Inherits="ReportHq.IncidentReports.People.Controls.GridForms.VictimForm" %>

<div class="gridForm">

	<table border="0" style="margin-bottom:10px;">
		<tr>
			<td class="label">Victim #: <span class="required">*</span></td>
			<td>
				<Telerik:RadNumericTextBox id="rntbVictimNumber" Type="Number" MaxLength="2" NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator="" Text='<%# Eval("VictimNumber") %>' Width="50" TabIndex="1" runat="server" />
				<asp:RequiredFieldValidator ID="rfvVictimNumber" ControlToValidate="rntbVictimNumber" ValidationGroup="person" ErrorMessage="Victim Number required" Display="none" runat="server" />
			</td>
			<td style="width:25px;"></td>
			<td class="label">SSN:</td>
			<td><Telerik:RadMaskedTextBox id="rmtbSsn" Mask="###-##-####" Text='<%# Eval("SocialSecurityNumber") %>' Width="100" TabIndex="14" runat="server" /></td>
		</tr>	
		<tr>
			<td class="label">Type: <span class="required">*</span></td>
			<td>
				<Telerik:RadComboBox ID="rcbVictimPersonType" DataValueField="VictimPersonTypeID" DataTextField="Description" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" Width="180" TabIndex="2" runat="server" />
				<asp:RequiredFieldValidator ID="rfvVictimPersonType" ControlToValidate="rcbVictimPersonType" ValidationGroup="person" ErrorMessage="Type required" Display="none" runat="server" />
			</td>
			<td></td>
			<td class="label">Drivers Lic:</td>
			<td><asp:TextBox ID="tbDriversLicense" Text='<%# Eval("DriversLicenseNumber") %>' Width="96" TabIndex="15" runat="server" /></td>
		</tr>	
		<tr>
			<td class="label">Victim Type:</td>
			<td><Telerik:RadComboBox ID="rcbVictimTypes" DataValueField="VictimTypeID" DataTextField="Description" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" Width="180" TabIndex="3" runat="server" /></td>
			<td></td>
			<td class="label">Sobriety:</td>
			<td><Telerik:RadComboBox ID="rcbSobrietyTypes" DataValueField="SobrietyTypeID" DataTextField="Description" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" Width="180" TabIndex="16" runat="server" /></td>
		</tr>	
		<tr>
			<td class="label">Last Name:</td>
			<td><asp:TextBox ID="tbLastName" Text='<%# Eval("LastName") %>' Width="176" TabIndex="4" runat="server" /></td>
			<td></td>
			<td class="label">Injury:</td>
			<td><Telerik:RadComboBox ID="rcbInjuryTypes" DataValueField="InjuryTypeID" DataTextField="Description" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" Width="180" TabIndex="17" runat="server" /></td>
		</tr>	
		<tr>
			<td class="label">First Name:</td>
			<td><asp:TextBox ID="tbFirstName" Text='<%# Eval("FirstName") %>' Width="96" TabIndex="5" runat="server" /></td>
			<td></td>
			<td class="label">Treated:</td>
			<td><Telerik:RadComboBox ID="rcbTreatedTypes" DataValueField="TreatedTypeID" DataTextField="Description" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" Width="180" TabIndex="18" runat="server" /></td>
		</tr>	
		<tr>
			<td class="label">Race:</td>
			<td><Telerik:RadComboBox ID="rcbRace" DataValueField="RaceTypeID" DataTextField="Description" Width="100" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" TabIndex="6" runat="server" /></td>
			<td></td>
			<td class="label">Occupation:</td>
			<td><asp:TextBox ID="tbOccupation" Text='<%# Eval("Occupation") %>' Width="176" TabIndex="19" runat="server" /></td>
		</tr>	
		<tr>
			<td class="label">Sex:</td>
			<td><Telerik:RadComboBox ID="rcbGender" DataValueField="GenderTypeID" DataTextField="Description" Width="100" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" TabIndex="7" runat="server" /></td>
			<td></td>
			<td class="label">Work Addr:</td>
			<td><asp:TextBox ID="tbWorkStreetAddress" Text='<%# Eval("WorkStreetAddress") %>' Width="176" TabIndex="20" runat="server" /></td>
		</tr>	
		<tr>
			<td class="label">DOB:</td>
			<td><Telerik:RadDateInput ID="rdiDateOfBirth" DbSelectedDate='<%# Eval("DateOfBirth") %>' MinDate="01/01/1900" Width="100" TabIndex="8" runat="server" /></td>
			<td></td>
			<td class="label">City:</td>
			<td><asp:TextBox ID="tbWorkCity" Text='<%# Eval("WorkCity") %>' Width="176" TabIndex="21" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">Home Addr:</td>
			<td><asp:TextBox ID="tbStreetAddress" Text='<%# Eval("StreetAddress") %>' Width="176" TabIndex="9" runat="server" /></td>
			<td></td>
			<td class="label">State:</td>
			<td><Telerik:RadComboBox ID="rcbWorkState" DataValueField="Abbreviation" DataTextField="State" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" Width="180" TabIndex="22" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">City:</td>
			<td><asp:TextBox ID="tbCity" Text='<%# Eval("City") %>' Width="176" TabIndex="10" runat="server" /></td>
			<td></td>
			<td class="label">Zip Code:</td>
			<td><Telerik:RadNumericTextBox id="rntbWorkZipCode" Type="Number" MaxLength="5" NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator="" Text='<%# Eval("WorkZipCode") %>' Width="100" TabIndex="23" runat="server" /></td>
		</tr>	
		<tr>
			<td class="label">State:</td>
			<td><Telerik:RadComboBox ID="rcbState" DataValueField="Abbreviation" DataTextField="State" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" Width="180" TabIndex="11" runat="server" /></td>
			<td></td>
			<td class="label">Work Phone:</td>
			<td><Telerik:RadMaskedTextBox id="rmtbWorkPhone" Mask="(###) ###-#### ext: ####" Text='<%# Eval("WorkPhoneNumber") %>' Width="180" TabIndex="24" runat="server" /></td>
		</tr>	
		<tr>
			<td class="label">Zip Code:</td>
			<td><Telerik:RadNumericTextBox id="rmtbZipCode" Type="Number" MaxLength="5" NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator="" Text='<%# Eval("ZipCode") %>' Width="100" TabIndex="12" runat="server" /></td>
			<td></td>
			<td class="label">Email:</td>
			<td>
				<asp:TextBox ID="tbEmail1" Text='<%# Eval("EmailAddress") %>' Width="176" TabIndex="25" runat="server" />
				<asp:RegularExpressionValidator 
					id="revEmail1" 
					ControlToValidate="tbEmail1" 
					ErrorMessage="Email invalid"
					ValidationGroup="person"					
					ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 						
					Display="none" 
					runat="server" />
			</td>
		</tr>
		<tr>
			<td class="label">Home Phone:</td>
			<td colspan="4"><Telerik:RadMaskedTextBox id="rmtbPhone" Mask="(###) ###-####" Text='<%# Eval("PhoneNumber") %>' Width="100" TabIndex="13" runat="server" /></td>
		</tr>	
	</table>

	<asp:ValidationSummary ID="validationSummary" ValidationGroup="person" DisplayMode="bulletList" CssClass="validationSummary" HeaderText="Problems:" EnableClientScript="true" runat="server" />
		
	<asp:Button id="btnInsert" CommandName="PerformInsert" text="Save Person" ValidationGroup="person" Visible='<%# DataItem is Telerik.Web.UI.GridInsertionObject %>' TabIndex="27" runat="server" />&nbsp;
	<asp:Button id="btnUpdate" CommandName="Update" text="Update" ValidationGroup="person" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>' TabIndex="27" runat="server" />
	<asp:Button id="btnCancel" CommandName="Cancel" text="Cancel" CausesValidation="False" TabIndex="28" runat="server"/>

</div>