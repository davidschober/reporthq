﻿<%@ Page Title="Report HQ - Offenders" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="Offenders.aspx.cs" Inherits="ReportHq.IncidentReports.People.Offenders" %>
<%@ Register TagPrefix="ReportHq" TagName="Offenders" src="controls/Offenders.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="../css/reportForm.css" type="text/css" />
	<script type="text/javascript" src="../js/radComboBoxHelpers.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<ReportHq:ReportMainNav id="ucReportMainNav" CurrentSection="People" OnNavItemClicked="ucMajorSections_NavItemClicked" runat="server" />

	<ReportHq:Offenders ID="ucOffenders" runat="server" />

</asp:Content>
