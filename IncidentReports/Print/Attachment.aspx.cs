﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Bases;

namespace ReportHq.IncidentReports.Print
{
	public partial class Attachment : Page
	{
		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			string fileType = Request.QueryString["fileType"];
			string filesFolder = System.Configuration.ConfigurationManager.AppSettings["FilesFolder"];
			string filePath = Request.QueryString["file"];
			string fullPath = Path.Combine(filesFolder, filePath);

			byte[] pdf = FileBuddy.GetFile(fullPath);

			string contentType = string.Empty;

			switch (fileType)
			{
				case "jpg" :
					contentType = "image/jpeg";
					break;
				case "pdf" :
					contentType = "application/pdf";
					break;				
			}

			Response.Clear();
			Response.ContentType = contentType;
			Response.AddHeader("content-disposition", "attachment; filename=" + Path.GetFileName(fullPath));
			Response.AddHeader("content-length", pdf.Length.ToString());
			Response.BinaryWrite(pdf);
			Response.End();
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}