﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Bases;
using ReportHq.IncidentReports.Controls;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.Property.Controls
{
	public partial class PropertyEvidence : System.Web.UI.UserControl
	{
		#region Variables

		private int _reportID;
		ReportPage _reportPage;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Properties

		public String ReceiptNumber
		{
			get { return tbReceiptNumber.Text; }
			set { tbReceiptNumber.Text = value; }
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			_reportPage = (ReportPage)this.Page;
			_reportPage.SyncSessionToViewState();

			_reportID = _reportPage.Report.ID;

			if (!_reportPage.UserHasWriteAccess)
			{
				//change "Edit" to "View"
				((GridEditCommandColumn)gridProperty.Columns[0]).EditText = "View";
				((GridEditCommandColumn)gridEvidence.Columns[0]).EditText = "View";

				//hide "Delete"
				gridProperty.Columns[1].Visible = false;
				gridEvidence.Columns[1].Visible = false;

				//disable Add command
				gridProperty.MasterTableView.CommandItemDisplay = GridCommandItemDisplay.None;
				gridEvidence.MasterTableView.CommandItemDisplay = GridCommandItemDisplay.None;
			}

			Label lbMessage = (Label)_reportPage.Master.FindControl("lblMessage");

			//setting definitions in code-behind so we can get the master page's label
			ajaxManager.AjaxSettings.AddAjaxSetting(gridProperty, gridProperty); //grid updates itself
			ajaxManager.AjaxSettings.AddAjaxSetting(gridProperty, lbMessage); //grid updates label
			ajaxManager.AjaxSettings.AddAjaxSetting(gridEvidence, gridEvidence); //grid updates itself
			ajaxManager.AjaxSettings.AddAjaxSetting(gridEvidence, lbMessage); //grid updates label
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#region Property Grid

		/// <summary>
		/// Fired when grid needs a datasource. eg, after editing or deleting.
		/// </summary>
		protected void gridProperty_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
		{
			//get table of current data from db
			gridProperty.DataSource = PropertyItem.GetPropertyByReport(_reportID);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void gridProperty_InsertCommand(object source, GridCommandEventArgs e)
		{
			//get values into hashtable
			Hashtable newValues = GetFormValues((GridForms.PropertyEvidenceForm)e.Item.FindControl(GridEditFormItem.EditFormUserControlID));

			PropertyItem property = new PropertyItem(newValues);
			property.ReportID = _reportID;

			StatusInfo status = property.AddProperty(HttpContext.Current.User.Identity.Name);

			if (!status.Success)
				_reportPage.RenderUserMessage(Enums.UserMessageTypes.Warning, ("Sorry, there was a problem saving: " + status.Message));
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void gridProperty_UpdateCommand(object sender, GridCommandEventArgs e)
		{
			GridEditableItem editableItem = (GridEditableItem)e.Item;
			int propertyID = (int)editableItem.GetDataKeyValue("PropertyID");

			//get values into hashtable
			Hashtable newValues = GetFormValues((GridForms.PropertyEvidenceForm)e.Item.FindControl(GridEditFormItem.EditFormUserControlID));

			PropertyItem property = new PropertyItem(newValues);
			property.ID = propertyID;

			StatusInfo status = property.UpdateProperty(HttpContext.Current.User.Identity.Name);

			if (!status.Success)
				_reportPage.RenderUserMessage(Enums.UserMessageTypes.Warning, ("Sorry, there was a problem saving: " + status.Message));
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void gridProperty_DeleteCommand(object sender, GridCommandEventArgs e)
		{
			GridEditableItem editableItem = (GridEditableItem)e.Item;
			int propertyID = (int)editableItem.GetDataKeyValue("PropertyID");

			StatusInfo status = PropertyItem.DeleteProperty(propertyID);

			if (!status.Success)
				_reportPage.RenderUserMessage(Enums.UserMessageTypes.Warning, ("Sorry, there was a problem saving: " + status.Message));
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Evidence Grid

		/// <summary>
		/// Fired when grid needs a datasource. eg, after editing or deleting.
		/// </summary>
		protected void gridEvidence_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
		{
			//get table of current data from db
			gridEvidence.DataSource = EvidenceItem.GetEvidenceByReport(_reportID);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void gridEvidence_InsertCommand(object source, GridCommandEventArgs e)
		{
			//get values into hashtable
			Hashtable newValues = GetFormValues((GridForms.PropertyEvidenceForm)e.Item.FindControl(GridEditFormItem.EditFormUserControlID));

			EvidenceItem evidence = new EvidenceItem(newValues);
			evidence.ReportID = _reportID;

			StatusInfo status = evidence.AddEvidence(HttpContext.Current.User.Identity.Name);

			if (!status.Success)
				_reportPage.RenderUserMessage(Enums.UserMessageTypes.Warning, ("Sorry, there was a problem saving: " + status.Message));
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void gridEvidence_UpdateCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
		{
			GridEditableItem editableItem = (GridEditableItem)e.Item;
			int evidenceID = (int)editableItem.GetDataKeyValue("EvidenceID");

			//get values into hashtable
			Hashtable newValues = GetFormValues((GridForms.PropertyEvidenceForm)e.Item.FindControl(GridEditFormItem.EditFormUserControlID));

			EvidenceItem evidence = new EvidenceItem(newValues);
			evidence.ID = evidenceID;

			StatusInfo status = evidence.UpdateEvidence(HttpContext.Current.User.Identity.Name);

			if (!status.Success)
				_reportPage.RenderUserMessage(Enums.UserMessageTypes.Warning, ("Sorry, there was a problem saving: " + status.Message));
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void gridEvidence_DeleteCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
		{
			GridEditableItem editableItem = (GridEditableItem)e.Item;
			int evidenceID = (int)editableItem.GetDataKeyValue("EvidenceID");

			StatusInfo status = EvidenceItem.DeleteEvidence(evidenceID);

			if (!status.Success)
				_reportPage.RenderUserMessage(Enums.UserMessageTypes.Warning, ("Sorry, there was a problem saving: " + status.Message));
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucReportSubNav_NavItemClicked(object sender, EventArgs e)
		{
			_reportPage.ProcessNavClick((sender as ReportSubNav).TargetRedirectUrl, false);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		private Hashtable GetFormValues(GridForms.PropertyEvidenceForm userControl)
		{
			Hashtable newValues = new Hashtable();
			string userStringValue;
			double? userDoubleValue;


			//loss type
			userStringValue = (userControl.FindControl("rcbLossTypes") as RadComboBox).SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["LossTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["LossTypeID"] = -1;

			//quantity
			userDoubleValue = (userControl.FindControl("rntbQuantity") as RadNumericTextBox).Value;
			if (userDoubleValue > 0)
				newValues["Quantity"] = Convert.ToDecimal(userDoubleValue);
			else
				newValues["Quantity"] = -1;

			//prop type
			userStringValue = (userControl.FindControl("rcbPropertyTypes") as RadComboBox).SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["TypeID"] = Int32.Parse(userStringValue);
			else
				newValues["TypeID"] = -1;

			//narc weight
			userStringValue = (userControl.FindControl("rcbNarcWeightTypes") as RadComboBox).SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["NarcoticWeightTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["NarcoticWeightTypeID"] = -1;

			//narc type
			userStringValue = (userControl.FindControl("rcbNarcTypes") as RadComboBox).SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["NarcoticTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["NarcoticTypeID"] = -1;

			newValues["Description"] = (userControl.FindControl("tbDescription") as TextBox).Text;
			newValues["Brand"] = (userControl.FindControl("tbBrand") as TextBox).Text;
			newValues["SerialNumber"] = (userControl.FindControl("tbSerialNumber") as TextBox).Text;

			//value
			userDoubleValue = (userControl.FindControl("rntbValue") as RadNumericTextBox).Value;
			if (userDoubleValue > 0)
				newValues["Value"] = Convert.ToInt32(userDoubleValue);
			else
				newValues["Value"] = -1;

			newValues["ReceiptNumber"] = tbReceiptNumber.Text;


			return newValues;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}