﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Weapons.ascx.cs" Inherits="ReportHq.IncidentReports.Property.Controls.Weapons" %>
<%@ Register TagPrefix="ReportHq" TagName="ReportSubNav" src="ReportSubNav.ascx" %>

<ReportHq:ReportSubNav ID="ucReportSubNav" CurrentSection="Weapons" OnNavItemClicked="ucReportSubNav_NavItemClicked" runat="server" />
	
<div class="formBox" style="width:640px;">

	<Telerik:RadGrid ID="gridWeapons" 
		AllowPaging="false"
		AllowSorting="true" 
		AutoGenerateColumns="False"
		AllowMultiRowSelection="true"
		OnNeedDataSource="gridWeapons_NeedDataSource" 
		OnInsertCommand="gridWeapons_InsertCommand"
		OnUpdateCommand="gridWeapons_UpdateCommand" 
		OnDeleteCommand="gridWeapons_DeleteCommand"
		runat="server">

		<ClientSettings 
			EnableRowHoverStyle="true"
			AllowDragToGroup="false" 
			AllowColumnsReorder="false" 
			AllowKeyboardNavigation="true"
			ReorderColumnsOnClient="false" >
			<Resizing AllowColumnResize="false" AllowRowResize="false" />
			<Selecting AllowRowSelect="false" />
		</ClientSettings>
			
		<MasterTableView 
			CommandItemDisplay="Top" 
			DataKeyNames="WeaponID" 
			EditMode="EditForms" 
			NoMasterRecordsText="No Weapons to display." 
			HeaderStyle-ForeColor="#191970" 
			ItemStyle-ForeColor="black" 
			AlternatingItemStyle-ForeColor="black">
				
			<CommandItemSettings AddNewRecordText="Add Weapon" ShowRefreshButton="false" />
			<CommandItemStyle CssClass="commandItem" />
				
			<Columns>
				<Telerik:GridEditCommandColumn ButtonType="LinkButton" UniqueName="editColumn" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="commandItem" />
				<Telerik:GridButtonColumn CommandName="Delete" ButtonType="LinkButton" Text="Delete" UniqueName="deleteColumn" ConfirmText="Delete Weapon?" ConfirmTitle="Delete" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="commandItem" />
				<Telerik:GridBoundColumn HeaderText="WeaponID" UniqueName="WeaponID" DataField="WeaponID" ReadOnly="true" Visible="false"/>
				<Telerik:GridBoundColumn HeaderText="Type" UniqueName="Type" DataField="Type" />
				<Telerik:GridBoundColumn HeaderText="Make" UniqueName="Make" DataField="Make"  />
				<Telerik:GridBoundColumn HeaderText="Caliber" UniqueName="Caliber" DataField="Caliber" />
				<Telerik:GridBoundColumn HeaderText="Serial" UniqueName="SerialNumber" DataField="SerialNumber" HeaderStyle-Wrap="false"/>
			</Columns>
				
			<EditFormSettings EditFormType="WebUserControl" UserControlName="controls/GridForms/WeaponForm.ascx" />
				
		</MasterTableView>
	</Telerik:RadGrid>

</div>

<telerik:RadAjaxManager ID="ajaxManager" DefaultLoadingPanelID="ajaxLoadingPanel" runat="server" />