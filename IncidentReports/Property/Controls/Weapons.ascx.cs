﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Bases;
using ReportHq.IncidentReports.Controls;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.Property.Controls
{
	public partial class Weapons : System.Web.UI.UserControl
	{
		#region Variables

		private int _reportID;
		ReportPage _reportPage;

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			_reportPage = (ReportPage)this.Page;
			_reportPage.SyncSessionToViewState();

			_reportID = _reportPage.Report.ID;

			if (!_reportPage.UserHasWriteAccess)
			{
				//change "Edit" to "View"
				((GridEditCommandColumn)gridWeapons.Columns[0]).EditText = "View";

				//hide "Delete"
				gridWeapons.Columns[1].Visible = false;

				//disable Add command
				gridWeapons.MasterTableView.CommandItemDisplay = GridCommandItemDisplay.None;
			}

			//setting definitions in code-behind so we can get the master page's label
			ajaxManager.AjaxSettings.AddAjaxSetting(gridWeapons, gridWeapons); //grid updates itself
			ajaxManager.AjaxSettings.AddAjaxSetting(gridWeapons, _reportPage.Master.FindControl("lblMessage")); //grid updates label
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Fired when grid needs a datasource. eg, after editing or deleting. 
		/// </summary>
		protected void gridWeapons_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
		{
			//get table of current weapons from db
			gridWeapons.DataSource = Weapon.GetWeaponsByReport(_reportID);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void gridWeapons_InsertCommand(object source, GridCommandEventArgs e)
		{
			//get values into hashtable
			Hashtable newValues = GetFormValues((GridForms.WeaponForm)e.Item.FindControl(GridEditFormItem.EditFormUserControlID));

			Weapon weapon = new Weapon(newValues);
			weapon.ReportID = _reportID;

			StatusInfo status = weapon.Add(HttpContext.Current.User.Identity.Name);

			if (!status.Success)
				_reportPage.RenderUserMessage(Enums.UserMessageTypes.Warning, ("Sorry, there was a problem saving: " + status.Message));
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void gridWeapons_UpdateCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
		{
			GridEditableItem editableItem = (GridEditableItem)e.Item;
			int weaponID = (int)editableItem.GetDataKeyValue("WeaponID");

			//get values into hashtable
			Hashtable newValues = GetFormValues((GridForms.WeaponForm)e.Item.FindControl(GridEditFormItem.EditFormUserControlID));

			Weapon weapon = new Weapon(newValues);
			weapon.ID = weaponID;

			StatusInfo status = weapon.Update(HttpContext.Current.User.Identity.Name);

			if (!status.Success)
				_reportPage.RenderUserMessage(Enums.UserMessageTypes.Warning, ("Sorry, there was a problem saving: " + status.Message));
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void gridWeapons_DeleteCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
		{
			GridEditableItem editableItem = (GridEditableItem)e.Item;
			int weaponID = (int)editableItem.GetDataKeyValue("WeaponID");

			StatusInfo status = Weapon.DeleteWeapon(weaponID);

			if (!status.Success)
				_reportPage.RenderUserMessage(Enums.UserMessageTypes.Warning, ("Sorry, there was a problem saving: " + status.Message));
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucReportSubNav_NavItemClicked(object sender, EventArgs e)
		{
			_reportPage.ProcessNavClick((sender as ReportSubNav).TargetRedirectUrl, false);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		private Hashtable GetFormValues(GridForms.WeaponForm userControl)
		{
			Hashtable newValues = new Hashtable();
			string userStringValue;

			newValues["FirearmTypeID"] = Int32.Parse((userControl.FindControl("rcbFirearmTypes") as RadComboBox).SelectedValue);

			//make
			userStringValue = (userControl.FindControl("rcbFirearmMakeTypes") as RadComboBox).SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["FireArmMakeTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["FireArmMakeTypeID"] = -1;

			//model
			newValues["Model"] = (userControl.FindControl("tbModel") as TextBox).Text;

			//caliber
			userStringValue = (userControl.FindControl("rcbFirearmCaliberTypes") as RadComboBox).SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["FirearmCaliberTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["FirearmCaliberTypeID"] = -1;

			newValues["SerialNumber"] = (userControl.FindControl("tbSerialNumber") as TextBox).Text;

			//ncic
			userStringValue = (userControl.FindControl("rblNcicIsStolen") as RadioButtonList).SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["NcicIsStolen"] = Convert.ToBoolean(userStringValue);
			else
				newValues["NcicIsStolen"] = false;

			//pawnshop
			userStringValue = (userControl.FindControl("rblPawnshopRecord") as RadioButtonList).SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["HasPawnshopRecord"] = Convert.ToBoolean(userStringValue);
			else
				newValues["HasPawnshopRecord"] = false;

			newValues["NcicContactName"] = (userControl.FindControl("tbNcicContactName") as TextBox).Text;
			newValues["PawnshopContactName"] = (userControl.FindControl("tbPawnshopContactName") as TextBox).Text;
			newValues["AdditionalInfo"] = (userControl.FindControl("tbAdditionalInfo") as TextBox).Text;

			return newValues;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}