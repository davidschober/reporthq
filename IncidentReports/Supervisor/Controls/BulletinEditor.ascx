﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BulletinEditor.ascx.cs" Inherits="ReportHq.IncidentReports.Supervisor.Controls.BulletinEditor" %>

<Telerik:RadFormDecorator id="radFormDecorator" DecoratedControls="CheckBoxes" Skin="Metro" runat="server" />

<table style="margin-bottom:4px;">
	<tr>
		<td class="label">Type:</td>
		<td>
			<asp:DropDownList ID="ddlTypes" DataValueField="ID" DataTextField="Description" Width="150" runat="server" />				
			<asp:RequiredFieldValidator ID="rfvType" ControlToValidate="ddlTypes" ValidationGroup="bulletin" ErrorMessage="Type required" Display="none" runat="server" />
			<asp:CheckBox ID="cbIsApb" Text="APB?" ToolTip="If checked, will send notice to all users" runat="server" />
		</td>
	</tr>
	<tr>
		<td class="label">Title:</td>
		<td>
			<asp:TextBox ID="tbTitle" Width="200" MaxLength="50" runat="server" />
			<asp:RequiredFieldValidator ID="rvfTitle" ControlToValidate="tbTitle" ValidationGroup="bulletin" ErrorMessage="Title required" Display="none" runat="server" />
		</td>
	</tr>
	<tr id="trActive" runat="server" visible="false">
		<td class="label">Active:</td>
		<td><asp:CheckBox ID="cbIsActive" runat="server" /></td>
	</tr>
</table>

<telerik:RadEditor id="radEditor" Width="830" Height="550" Skin="Default" EditModes="Design,Html,Preview" runat="server">
	<Tools>
		<telerik:EditorToolGroup>
			<telerik:EditorTool Name="TemplateManager" />
			<telerik:EditorTool Name="ImageManager" />				
			<telerik:EditorTool Name="DocumentManager" />
			<telerik:EditorTool Name="InsertExternalVideo" />
		</telerik:EditorToolGroup>

		<telerik:EditorToolGroup>
			<telerik:EditorTool Name="AjaxSpellCheck" />
			<telerik:EditorSeparator />
			<telerik:EditorTool Name="Cut" />
			<telerik:EditorTool Name="Copy" />
			<telerik:EditorTool Name="Paste" />
			<telerik:EditorTool Name="PasteFromWord" />
			<telerik:EditorSeparator />
			<telerik:EditorTool Name="Undo"	/>
			<telerik:EditorTool Name="Redo" />
		</telerik:EditorToolGroup>

		<telerik:EditorToolGroup>
			<telerik:EditorTool Name="JustifyLeft" />
			<telerik:EditorTool Name="JustifyCenter" />
			<telerik:EditorTool Name="JustifyRight" />
			<telerik:EditorSeparator />
			<telerik:EditorTool Name="Indent" />
			<telerik:EditorTool Name="Outdent" />
			<telerik:EditorSeparator />
			<telerik:EditorTool Name="InsertOrderedList" />
			<telerik:EditorTool Name="InsertUnorderedList" />
		</telerik:EditorToolGroup>

		<telerik:EditorToolGroup>
			<telerik:EditorTool Name="FontSize" />
			<telerik:EditorTool Name="ForeColor" />
			<telerik:EditorTool Name="BackColor" />
			<telerik:EditorTool Name="Bold" />
			<telerik:EditorTool Name="Italic" />
			<telerik:EditorTool Name="Underline" />
			<telerik:EditorSeparator />
			<telerik:EditorTool Name="FormatStripper" />
			<telerik:EditorSeparator />
			<telerik:EditorTool Name="ConvertToUpper" />
			<telerik:EditorTool Name="ConvertToLower" />
		</telerik:EditorToolGroup>

		<telerik:EditorToolGroup>
			<telerik:EditorTool Name="InsertTable" />
		</telerik:EditorToolGroup>

	</Tools>

	<TemplateManager 
		ViewPaths="~/RadEditorFiles/Bulletins/Templates/"
		UploadPaths="~/RadEditorFiles/Bulletins/Templates/" />

	<ImageManager 
		ViewPaths="~/RadEditorFiles/Bulletins/Images/"
		UploadPaths="~/RadEditorFiles/Bulletins/Images/" />

	<DocumentManager
		ViewPaths="~/RadEditorFiles/Bulletins/Documents/"
		UploadPaths="~/RadEditorFiles/Bulletins/Documents/" />
</telerik:RadEditor><br />

<span class="label">Recipients:</span><br />
<asp:CheckBoxList ID="cblUserRoles" DataValueField="ID" DataTextField="Name" runat="server" /><br />
<!-- TODO: custom validation for cbl -->

<asp:ValidationSummary ID="validationSummary" ValidationGroup="bulletin" DisplayMode="bulletList" CssClass="validationSummary" HeaderText="Problems:" EnableClientScript="true" runat="server" />
