﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports.Controls;

namespace ReportHq.IncidentReports.Supervisor
{
	public partial class Default : Bases.NormalPage
	{
		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			this.Heading = "Supervisor Tools";
			this.HelpUrl = "~/Supervisor/Help/Default.html";

			rcPendingReports.DataSource = Reporting.PendingDistrictReports();
			rcRejectedReports.DataSource = Reporting.RejectedDistrictReports();

			//TEMP: using older date for data since db is old. should be 90-days
			//rcApprovedReports.DataSource = Reporting.ApprovedReportsByDistrict(DateTime.Now.AddDays(-90), DateTime.Now);
			rcApprovedReports.DataSource = Reporting.ApprovedReportsByDistrict(new DateTime(2013, 7, 1), new DateTime(2013, 10, 1));

			rcTotalReports.DataSource = Reporting.DistrictReportsPercentage();			
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}