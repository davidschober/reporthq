﻿<%@ Page Title="Manage Bulletins" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="ManageBulletins.aspx.cs" Inherits="ReportHq.IncidentReports.Supervisor.ManageBulletins" %>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<a href="CreateBulletin.aspx" title="Create a new Bulletin" class="flatButton"><i class="fa fa-plus fa-fw"></i> Create Bulletin</a><br /><br />
	
	<p><asp:Label ID="lblRecordCount" runat="server" /></p>

	<Telerik:RadAjaxPanel ID="updResults" LoadingPanelID="ajaxLoadingPanel" runat="server">
				
		<%--
			OnItemUpdated="RadGrid1_ItemUpdated"
			OnBatchEditCommand="RadGrid1_BatchEditCommand"--%>
			
		<Telerik:RadGrid ID="gridBulletins" 
			width="95%"
			AutoGenerateColumns="False"
			AllowPaging="true"
			PageSize="25" 
			PagerStyle-Mode="NextPrevAndNumeric" 
			PagerStyle-Position="TopAndBottom"			
			AllowSorting="true" 
			AllowMultiRowSelection="true"  
			AllowMultiRowEdit="true" 
			OnNeedDataSource="gridBulletins_NeedDataSource" 
			OnItemUpdated="gridBulletins_ItemUpdated"
			
			runat="server">

			<ClientSettings 
				EnableRowHoverStyle="true"
				AllowDragToGroup="false" 
				AllowColumnsReorder="false" 
				AllowKeyboardNavigation="true"
				ReorderColumnsOnClient="false" >
				<Resizing AllowColumnResize="false" AllowRowResize="false" />
				<Selecting AllowRowSelect="false" EnableDragToSelectRows="false" />
			</ClientSettings>
			
			<MasterTableView 
				CommandItemDisplay="Bottom" 
				DataKeyNames="ID" 
				NoMasterRecordsText="No Bulletins to display." 
				CommandItemStyle-Font-Bold="true" 
				HeaderStyle-ForeColor="#191970" 
				ItemStyle-CssClass="item" 
				AlternatingItemStyle-CssClass="item"
				EditMode="Batch">
				
				<CommandItemSettings 
					ShowSaveChangesButton="true"
					ShowCancelChangesButton="true"
					ShowRefreshButton="true" 
					ShowAddNewRecordButton="false" />

				<BatchEditingSettings EditType="Cell" />
				
				<Columns>				
					<Telerik:GridHyperLinkColumn UniqueName="Edit" DataNavigateUrlFields="ID" DataNavigateUrlFormatString="EditBulletin.aspx?bid={0}" DataTextField="ID" DataTextFormatString="Edit" HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="center" ItemStyle-Wrap="false" ItemStyle-CssClass="commandItem" />
					<Telerik:GridBoundColumn DataField="ID" Visible="false" />				
					<Telerik:GridCheckBoxColumn DataField="IsActive" UniqueName="IsActive" HeaderText="Active" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
					
					<%--<Telerik:GridTemplateColumn UniqueName="IsActive2" HeaderText="Active" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" >						
						<ItemTemplate>
							<input id="Checkbox1" type="checkbox" checked='< %# Eval("IsActive") % >' onclick="changeEditor(this);" />
						</ItemTemplate>
						<EditItemTemplate>
							<asp:CheckBox ID="CheckBox2" runat="server" Checked='< %# Bind("IsActive") % >' />
						</EditItemTemplate>
					</Telerik:GridTemplateColumn>--%>
					
					<Telerik:GridBoundColumn DataField="CreatedDate" UniqueName="CreatedDate" HeaderText="Created" DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-Width="75px" />
					<Telerik:GridBoundColumn DataField="Type" UniqueName="Type" HeaderText="Type" HeaderStyle-Width="110px" ItemStyle-Wrap="false" />
					<Telerik:GridCheckBoxColumn DataField="IsAllPoints" UniqueName="IsApb" HeaderText="APB" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
					<Telerik:GridBoundColumn DataField="Title" UniqueName="Title" HeaderText="Title" ItemStyle-Wrap="false" />
					<Telerik:GridBoundColumn DataField="CreatedBy" UniqueName="CreatedBy" HeaderText="CreatedBy" ItemStyle-Wrap="false" />
				</Columns>
				
			</MasterTableView>
		</Telerik:RadGrid>

	</Telerik:RadAjaxPanel>

	<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
		<script type="text/javascript">

			function changeEditor(checkbox) {
				var grid = $find("<%= gridBulletins.ClientID %>");
				var batchManager = grid.get_batchEditingManager();

				//opens the checkbox cell
				grid.get_batchEditingManager().openCellForEdit(checkbox.parentElement.parentElement);

				checkbox.checked = !checkbox.checked;
			}

		</script>
	</telerik:RadCodeBlock>
	

</asp:Content>
