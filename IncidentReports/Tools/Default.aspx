﻿<%@ Page Title="Tools" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ReportHq.IncidentReports.Tools.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<div style="width:200px; margin-left:auto; margin-right:auto;">
	
		<p><a href="FindReports.aspx" title="Find Reports" class="flatButton med" style="width:215px;"><i class="fa fa-search fa-fw"></i> Find Reports</a></p>
		<p><a href="FindOffenders.aspx" title="Find Offenders" class="flatButton med" style="width:215px;"><i class="fa fa-search fa-fw"></i> Find Offenders</a></p>
		<p><a href="PlotReports.aspx" title="Plot Reports on a map" class="flatButton med" style="width: 215px;"><i class="fa fa-map-marker fa-fw"></i>Plot Reports</a></p>
		<p><a href="#" title="Change a Report's Item Number" class="flatButton med" style="width:215px;"><i class="fa fa-gear fa-fw"></i> Change Item Number</a></p>
			
	</div>

</asp:Content>
