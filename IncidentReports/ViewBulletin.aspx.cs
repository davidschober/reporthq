﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Controls;

namespace ReportHq.IncidentReports
{
	public partial class ViewBulletin : Page
	{
		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			Bulletin bulletin = new Bulletin(Int32.Parse(Request.QueryString["bid"]));

			if (bulletin.IsActive)
				litBulletin.Text = bulletin.BulletinContent;
			else
				litBulletin.Text = "This Bulletin is not active. Only active Bulletins can be viewed.";
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}