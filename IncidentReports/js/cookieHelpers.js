//http://techpatterns.com/downloads/javascript_cookies.php

//sets name/value cookie in document cookie
function setCookie(name, value, expDate, path, domain, secure)
{
	var cookieString = name + '=' + escape(value) +
		((expDate) ? ';expires=' + expDate.toGMTString() : '') + 
		((path) ? ';path=' + path : '') + 
		((domain) ? ';domain=' + domain : '') +
		((secure) ? ';secure' : '');
	//alert('set cookie: ' + cookieString);
	
	document.cookie = cookieString; // adds our mini-cookie
}

//gets a value from document cookie
function getCookie(checkName)
{
	// first split document cookie into name/value pairs
	// note: document.cookie only returns name=value, no other attributes
	var allCookies = document.cookie.split(';');

	//loop vars
	var cookie = '';
	var cookieName = '';
	var cookieValue = '';
	var cookieFound = false;
	
	for (i=0; i < allCookies.length; i++ )
	{
		//now split apart each name=value pair
		cookie = allCookies[i].split( '=' );
				
		//trim left/right whitespace
		cookieName = cookie[0].replace(/^\s+|\s+$/g, '');
	
		if (cookieName == checkName)
		{
			cookieFound = true;
			
			//handle cases where cookie exists but has no value
			if ( cookie.length > 1 )
			{
				cookieValue = unescape( cookie[1].replace(/^\s+|\s+$/g, '') );
			}
			
			//note: if cookie is initialized but has no value, null is returned
			return cookieValue;
			break;
		}
		
		cookie = null;
		cookieName = '';
	}
	
	if (!cookieFound)
	{
		return null;
	}
}

//deletes a cookie
function deleteCookie(name, path, domain)
{
	if (getCookie(name))
	{
		var cookieString = name + '=' +
			((path) ? ';path=' + path : '') +
			((domain) ? ";domain=" + domain : '') +
			';expires=Thu, 01-Jan-1970 00:00:01 GMT';
		//alert(cookieString);
			
		document.cookie = cookieString;
	}
}