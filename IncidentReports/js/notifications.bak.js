﻿var uri = '/api/notifications';
var username;


//var globalNotificationItemIDs = '<Items>';
//var reportNotificationItemIDs = '<Items>';

//var newGlobalNotifications = 0;
//var newReportNotifications = 0;

//hidden elements for tracking notificationIDs to mark as read
var globalNotificationItemIDs;
var reportNotificationItemIDs;

//counts for badge display
var newGlobalNotifications;
var newReportNotifications;

//dunno why but these dont work when pulled in functions.
//var GlobalNotifications = $('#GlobalNotifications');
//var ReportNotifications = $('#ReportNotifications');

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

//on document ready, create a new function to hit our service

$(document).ready(getNotifications);

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function getNotifications() {

	username = document.getElementById('hidUsername').value;
	//alert(username);

	globalNotificationItemIDs = '<Items>';
	reportNotificationItemIDs = '<Items>';

	newGlobalNotifications = 0;
	newReportNotifications = 0;

	//alert($('#Notifications').is(":visible"));

	//remove old notifications
	$('#GlobalNotifications').empty();
	$('#ReportNotifications').empty();
	
	getGlobalNotifications();
	getReportNotifications();

	//repeat every X seconds (10000 = 10s)
	setTimeout(getNotifications, 30000);
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function getGlobalNotifications() {

	//get ajax request to uri
	$.getJSON(uri + '/global/' + username)

		.done(function (data) {
			//success; 'data' now contains a typed list of notifications

			var GlobalNotifications = $('#GlobalNotifications'); //dunno why by a global var fails, must retrieve here
			var GlobalNotificationsHeader = $('#GlobalNotificationsHeader')

			if (data != null && data.length > 0) //got results, render
			{
				GlobalNotificationsHeader.show();
				GlobalNotifications.show();

				//newGlobalNotifications = data.length; //not doing since we only want non-viewed APBs. viewed-but-under-24-hours are included in results

				$.each(data, function (key, item) {

					//we do different things for viewed/unviewed notifs
					if (item.Viewed == false) {
						//only badge non-viewed items. this allows viewed-but-fresh APBs to be rendered but not badged.
						newGlobalNotifications = newGlobalNotifications + 1;

						//manage a list for mark-as-read. dont track .Viewed == true items.
						globalNotificationItemIDs = globalNotificationItemIDs + '<id>' + item.ID + '</id>';
					}

					//add a list item for each data item to the HTML element
					$(formatGlobalItem(item)).appendTo(GlobalNotifications);
				});

				globalNotificationItemIDs = globalNotificationItemIDs + '</Items>';
				//alert(globalNotificationItemIDs);

				$('#GlobalNotificationItemIds').val(globalNotificationItemIDs);
				//alert($('#GlobalNotificationItemIds').val());

				updateNotificationsCount();
			}
			else {
				//alert('no global results found')
				GlobalNotificationsHeader.hide();
				GlobalNotifications.hide();
			}
		})

	.fail(function () {
		//alert('request failed');
		$('#GlobalNotifications').hide();
		$('#GlobalNotificationsHeader').hide();
	})
	//.always(function () { alert('getJSON request ended'); })
	;
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function getReportNotifications() {
	
	//get ajax request to uri
	$.getJSON(uri + '/report/' + username)

		.done(function (data) {
			//success; 'data' now contains a typed list of notifications

			var ReportNotifications = $('#ReportNotifications');
			var EmptyNotifications = $('#EmptyNotifications');

			if (data != null && data.length > 0) //got results, render
			{
				ReportNotifications.show();
				EmptyNotifications.hide();

				newReportNotifications = data.length;

				$.each(data, function (key, item) {
					//add a list item for each data item to the HTML element
					$(formatReportItem(item)).appendTo(ReportNotifications);

					//manage a list for mark-as-read
					reportNotificationItemIDs = reportNotificationItemIDs + '<id>' + item.ID + '</id>';
				});

				reportNotificationItemIDs = reportNotificationItemIDs + '</Items>';
				//alert(reportNotificationItemIDs);

				$('#hidReportNotificationItemIds').val(reportNotificationItemIDs);
				//alert($('#ReportNotificationItemIds').val());

				updateNotificationsCount();
			}
			else {
				ReportNotifications.hide();
				EmptyNotifications.show();
			}
		})

		.fail(function () {
			//alert('getJSON request failed');
			$('#ReportNotifications').hide();
		})
		//.always(function () { alert('getJSON request ended'); })
	;
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function updateNotificationsCount() {
	var total = newReportNotifications + newGlobalNotifications;
	
	var spanNotificationsCount = $('#NotificationsCount');

	if (total > 0) {
		spanNotificationsCount.text(total);
		spanNotificationsCount.show();
	} else {
		spanNotificationsCount.hide();
	}
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function formatGlobalItem(item) {
	//alert(".ID: " + item.ID + " .Type: " + item.Type + " .Message: " + item.Message + " .ObjectID: " + item.ObjectID);
	//alert(".ID: " + item.ID + " .Url: " + item.Url + " .Message: " + item.Message + " .ObjectID: " + item.ObjectID);

	//TODO: this needs to be more flexible. maybe have the GlobalNotification() class build up the entire string?

	//return '<a href="/event/BasicInfo.aspx?rid=' + item.ObjectID + '">' + item.Message + '</a>';

	return '<a href="' + item.Url + '">' + item.Message + '</a>';
}

function formatReportItem(item) {
	//alert(".ID: " + item.ID + " .ReportID: " + item.ReportID + " .Message: " + item.Message);

	return '<a href="/event/BasicInfo.aspx?rid=' + item.ReportID + '">' + item.Message + '</a>';
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

//fire when click-closing the notifications panel
function markNotificationsAsRead()
{
	markGlobalNotificationsAsRead();
	markReportNotificationsAsRead();
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function markGlobalNotificationsAsRead(){
	//alert(globalNotificationItemIDs);

	if (globalNotificationItemIDs.length > 15) //"<Items></Items>"
	{
		$.ajax({
			type: "POST",
			url: uri + '/setGlobalAsRead/' + username,
			data: '=' + globalNotificationItemIDs, //for some reason Web API only maps complex objects, not text..unless prefixed w/ "="
			dataType: 'text',
			success: function (jqXHR, textStatus, errorThrown) {
				newGlobalNotifications = 0;
				updateNotificationsCount();
			},
			error: function (jqXHR, textStatus, errorThrown) {
				//alert('global mark as read failed');
			}
		});
	}
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function markReportNotificationsAsRead() {
	//alert(reportNotificationItemIDs);
	
	if (reportNotificationItemIDs.length > 15) //"<Items></Items>"
	{
		$.ajax({
			type: "POST",
			url: uri + '/setReportAsRead/' + username,
			data: '=' + reportNotificationItemIDs, //for some reason Web API only maps complex objects, not text..unless prefixed w/ "="
			dataType: 'text',
			success: function (jqXHR, textStatus, errorThrown) {
				newReportNotifications = 0;
				updateNotificationsCount();
			},
			error: function (jqXHR, textStatus, errorThrown) {
				//alert('report mark as read failed');
			}
		});
	}
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

//fired when user clicks notifications bell icon
function showNotifications()
{
	toggleMenu("#Notifications"); //toggle menu
	
	markGlobalNotificationsAsRead();
	markNotificationsAsRead();
}