// - used by various pages to recall whether certain RadWindows are open/closed, and where on the screen.
// - it is also used to maintain the applications Timeout for session renewal.

//'''''''''''''''''''''''''''''''''''''''''''''''''''

//this popup stores user's last-report ID, and allows user to renew it in Session
function openRenewSession()
{
	radopen(null, 'rwRenewSession');
}

//'''''''''''''''''''''''''''''''''''''''''''''''''''

//selectively opens a RadWindow, using cookies to record visibility state
function checkAndOpenRadWindow(windowName, visibleCookieName, coordsCookieName)
{
	var visible = getCookie(visibleCookieName);		
	
	if (visible && visible == 'True')
		openRadWindow(windowName, visibleCookieName, coordsCookieName);
}

//'''''''''''''''''''''''''''''''''''''''''''''''''''

//opens a RadWindow, using cookies to record visibility state
function openRadWindow(windowName, visibleCookieName, coordsCookieName) 
{
	var oWindow = radopen(null, windowName);	
	
	var coords = getCookie(coordsCookieName);
	if (coords)
	{
		var arrBounds = coords.split(',');
		oWindow.moveTo(arrBounds[0], arrBounds[1]);
	}
	
	//store visibility state in cookie for other-page use
	setCookie(visibleCookieName, 'True', '', '/', '', '');
}

//'''''''''''''''''''''''''''''''''''''''''''''''''''

//saves window's x,y. handles RadWindow's OnClientDragEnd
function saveWindowCoords(sender, args)
{
	var coordsCookieName = sender.get_element().getAttribute("CoordsCookieName"); //custom attribute
	      
	var bounds = $telerik.getBounds(sender.get_popupElement());
	
	//store coords in cookie for other-page use
	setCookie(coordsCookieName, bounds.x + ',' + bounds.y, '', '/', '', '');
}

//'''''''''''''''''''''''''''''''''''''''''''''''''''

//cleanup the trash - deletes a window's state cookies. handles a RadWindow's OnClientClose in RadWindowManager
function deleteVisCookies(sender, args)
{
	var visibleCookieName = sender.get_element().getAttribute("VisibleCookieName");
	var coordsCookieName = sender.get_element().getAttribute("CoordsCookieName");
	
	deleteCookie(visibleCookieName, '/', '');
	deleteCookie(coordsCookieName, '/', '');
}