﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports
{
	public partial class test : System.Web.UI.Page
	{
		
		protected void Page_Load(object sender, EventArgs e)
		{
			//temp - works
			//string[] itemsList = { "~/ReportAttachments/2014/7/G-99102-14/car.jpg", "~/ReportAttachments/2014/7/G-99102-14/car4.png", "~/ReportAttachments/2014/7/G-99102-14/car5.gif", "~/ReportAttachments/2014/7/G-99102-14/car6.png" };
			//lightBox.DataSource = itemsList;
			//lightBox.DataBind();

			//loading from folder - not working
			//lightBox.DataSource = GetLightboxFiles();
			//lightBox.DataBind();

			//records from db:
			//  ReportAttachments\2014\2\B-78889-14\car.jpg
			//


			DataTable dt = Attachment.GetAttachmentsByReportID(266016).Tables[0];

			List<string> itemsList = new List<string>();
			string itemPath;

			foreach (DataRow row in dt.Rows)
			{
				itemPath = (string)row["SavedPath"];

				//format from absolute to relative path
				itemPath = itemPath.Replace(@"\", "/").Insert(0, "~/");

				itemsList.Add(itemPath);
			}

			lightBox.DataSource = itemsList;
			lightBox.DataBind();


			//DataTable dt = Attachment.GetAttachmentsByReportID(266016).Tables[0];

			//lightBox.DataImageUrlField = "SavedPath";
			//lightBox.DataSource = dt;
			//lightBox.DataBind();
		
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		private object GetLightboxFiles()
		{
			List<LightboxFileInfo> lightboxFiles = new List<LightboxFileInfo>();

			foreach (string fileName in Directory.GetFiles(Server.MapPath("ReportAttachments/2014/7/G-99102-14")))
			{
				lightboxFiles.Add(new LightboxFileInfo
				{
					Path = "~\\" + fileName.Replace(Request.ServerVariables["APPL_PHYSICAL_PATH"], String.Empty), // "~\\ReportAttachments\\2014\\7\\G-99102-14\\car4.png"
					Name = Path.GetFileName(fileName), // "car4.png"
					NameNoExtension = Path.GetFileNameWithoutExtension(fileName) // "car4"
				});
			}

			//return list of file info needed by LightBox. problem: LB not showing the image
			return lightboxFiles;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	}

	//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	public class LightboxFileInfo
	{
		public string Path { get; set; }
		public string Name { get; set; }
		public string NameNoExtension { get; set; }
	}
}